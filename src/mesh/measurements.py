

import numpy as np
import scipy.ndimage as nd
import scipy.linalg as la

from numpy.linalg import svd, eig


def calculate_cell_covariance(verts, tris, labels):
    n = len(verts)
    va = np.zeros((n,), dtype=np.float32)
    vm = np.zeros((n, 3), dtype=np.float32)
    vc = np.zeros((n, 3, 3), dtype=np.float32)
    v_array=np.asarray(verts, dtype=np.float32) 
    tri_array=np.asarray(tris, dtype=int)
    tri_pts=v_array[tri_array]
    n = np.cross( tri_pts[:,1] - tri_pts[:,0], 
                  tri_pts[:,2] - tri_pts[:,0])
    ta = 0.5*np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
    tm = ta*np.sum(tri_pts, axis=1)/3.0
    M = np.array(( (2/12.0, 1/12.0, 1/12.0), (1/12.0, 2/12.0, 1/12.0), (1/12.0, 1/12.0, 2/12.0)))
    nt = tri_array.shape[0]
    tc = np.zeros((nt, 3, 3))
    for i in range(nt):
        tc[i,:,:] = ta[i]*np.dot(tri_pts[i, :, :].T, np.dot(M, tri_pts[i,:,:]))

    for i in range(len(tris)):
        va[tris[i]] += ta[i]/3.0
        vm[tris[i],:] += tm[i]
        vc[tris[i],:,:] += tc[i]


    return ca, cm, cc
    

def calculate_cell_areas_aniso(mesh, labels, return_cv=False, vertex_area=None):
 
    ca = {}
    mu = {}
        
    cv = defaultdict(list)
    for i, l in enumerate(labels):
        cv[l].append(i)

    print '#cells', len(cv)

    verts = mesh.verts
    tris = mesh.tris



    if return_cv:
        return ca, mu, cv
    else:
        return ca, mu


verts = [[ 0,0,0], [1,0,0],[0,1,0]]#[[0.2, 0.3, 0.4], [1, 0.34, 1], [0.2, 1.5, 0.464]]
tris = [[0, 1, 2]]

print calculate_vertex_area_covariance(verts, tris)
