

import numpy as np
import scipy.ndimage as nd
import scipy.linalg as la

from scipy.sparse import dok_matrix, csr_matrix
from scipy.spatial import cKDTree

from math import sqrt, ceil, floor, log, exp
from random import randint, choice, random

from scipy.interpolate import Rbf

import numpy.random as npr

from numpy.linalg import svd, eig

from collections import defaultdict

from eval_rbf import eval_rbf_linear

import heapq

from vertex_normals import calculate_vertex_normals as cvn

def calc_split_tris(verts, tris, labels, signal):
    new_verts = list(verts)
    new_tris = []
    new_labels = list(labels)
    new_signal = list(signal)
    edge_verts = {}

    Nv = len(new_verts)
    for tri_idx in tris:
        tri_labels = [labels[i] for i in tri_idx]
        N_labels = len(set(tri_labels))
        if N_labels>1:
            shared_labels = list(set(tri_labels))
            tri_verts = [verts[i] for i in tri_idx]
            c = np.mean(tri_verts, axis=0)
            new_verts.extend([c for _ in shared_labels])
            new_labels.extend(shared_labels)
            s = np.mean([signal[i] for i in tri_idx])
            new_signal.extend([s for _ in shared_labels])
            centre_map = dict(zip(shared_labels, range(Nv, Nv+N_labels)))
            Nv += N_labels
            for i in range(3):
                j = (i+1)%3
                k = (i+2)%3
                idx_j = tri_idx[j]
                idx_k = tri_idx[k]
                label_j = tri_labels[j]
                label_k = tri_labels[k]
                if label_j==label_k:
                    # No need to split this edge
                    new_tris.append((idx_j, idx_k, centre_map[label_j]))
                else:
                    # Need to split edge
                    try:
                        lj = edge_verts[(label_j, idx_j, idx_k)]
                    except KeyError:
                        ec = 0.5*(tri_verts[j] + tri_verts[k])
                        new_verts.append(ec)
                        es = 0.5*(signal[idx_j] + signal[idx_k])
                        new_signal.append(es)
                        new_labels.append(label_j)
                        edge_verts[(label_j, idx_j, idx_k)] = Nv
                        lj = Nv
                        Nv += 1
                    try:
                        lk = edge_verts[(label_k, idx_k, idx_j)]
                    except KeyError:
                        ec = 0.5*(tri_verts[j] + tri_verts[k])
                        new_verts.append(ec)
                        es = 0.5*(signal[idx_j] + signal[idx_k])
                        new_signal.append(es)
                        new_labels.append(label_k)
                        edge_verts[(label_k, idx_k, idx_j)] = Nv
                        lk = Nv
                        Nv += 1

                    new_tris.append((idx_j, lj, centre_map[label_j]))
                    new_tris.append((idx_k, centre_map[label_k], lk))
        else:
            new_tris.append(tri_idx)
    return new_verts, new_tris, new_labels, new_signal

def calculate_edge_lengths(mesh):
    n = len(mesh.verts)
    d = []
    dd = {}
    for i in range(mesh.n):
        for j in A.indicies[A.indptr[i]:A.indptr[i+1]]:
            l = la.norm(verts[i] - verts[j])
            dd[(i,j)] = l
            d.append(la.norm(verts[i] - verts[j]))
    return d, dd
    
def erode_labels(mesh, labels, in_place=True):
    if not in_place:
        labels = np.array(labels)
    bg = set(i for i,v in enumerate(labels) if v==0)
    A = mesh.connectivity
    for i in range(len(labels)):
        if labels[i]>0:
            for j in A.indices[A.indptr[i]:A.indptr[i+1]]:            
                if j in bg:
                    labels[i] = 0
                    break
    return labels


def calculate_vertex_normals(verts, tris):
    return cvn(verts.astype(np.float64), tris)
    """
    v_array = np.array(verts)
    tri_array = np.array(tris, dtype=int)
    tri_pts = v_array[tri_array]
    n = np.cross( tri_pts[:,1] - tri_pts[:,0], 
                  tri_pts[:,2] - tri_pts[:,0])


    v_normals = np.zeros(v_array.shape)

    for i in range(tri_array.shape[0]):
        for j in tris[i]:
            v_normals[j,:] += n[i,:]

    nrms = np.sqrt(v_normals[:,0]**2 + v_normals[:,1]**2 + v_normals[:,2]**2)
    
    v_normals = v_normals / nrms.reshape((-1,1))

    return v_normals
    """


def surface_watershed(mesh, signal, seeds, max_level=float('inf'), inplace=True):
    # On-mesh "watershed" to extend state labels to larger regions.
    # Not clear on exactly how to do this, so we'll pick a simple method
        
    # Priority queue for pixels (s[i], i) which neighbour
    # labelled pixels

    h = []

    labelled = 0
    Nv = len(mesh.verts)
    border = set()
    if not inplace:
        state = np.array(seeds)
    else:
        state = seeds
    A = mesh.connectivity
    # Loop over all pixels; for labelled pixels push unlabelled neighbours
    # into queue
    for i in range(Nv):
        if state[i]!=0:
            labelled +=1
            # Add nb vertices to queue
            for j in A.indices[A.indptr[i]:A.indptr[i+1]]:
                if j not in border:
                    heapq.heappush(h, (signal[j], j))
                    border.add(j)
    # Now pull pixel with smallest level, label with state of nb
    while h:
        l, i = heapq.heappop(h)
        border.remove(i)

        if l>max_level:
            break
        nbs = A.indices[A.indptr[i]:A.indptr[i+1]]
        n_states = state[nbs]
        n_signal = signal[nbs]
        state[i] = min((a,b) for a,b in zip(n_signal, n_states) if b!=0)[1]
        labelled +=1
        for j in A.indices[A.indptr[i]:A.indptr[i+1]]:
            if state[j]==0 and (j not in border):
                border.add(j)
                heapq.heappush(h, (signal[j], j))
    
    return state


def local_normalize(mesh, signal, r=20, sampling=0.002):
    verts = mesh.verts

    sub_idx = [i for i in range(len(verts)) if random()<sampling]
    sub_verts = [verts[i] for i in sub_idx]
    print "samples ", len(sub_verts)
    tree = cKDTree(verts)
    sub_min = np.zeros((len(sub_verts),), dtype=signal.dtype)
    sub_med = np.zeros((len(sub_verts),), dtype=signal.dtype)
    sub_max = np.zeros((len(sub_verts),), dtype=signal.dtype)
    min_threshold = np.percentile(signal, 20)
    med_threshold = np.percentile(signal, 50)
    max_threshold = np.max(signal)
    for i in range(len(sub_verts)):
        idx = tree.query_ball_point(sub_verts[i], r)
        d = signal[idx] #[v for v in signal[idx] if v>min_threshold]
        if len(d)>0:
            sub_min[i] = np.percentile(d, 20)
            sub_med[i] = np.percentile(d, 50)#signal[idx])
            sub_max[i] = np.max(d)#signal[idx])
        else:
            sub_min[i] = min_threshold
            sub_med[i] = med_threshold
            sub_max[i] = max_threshold
        if i%100==0:
            print '*', i, sub_med[i], sub_max[i]

    sub_med = np.maximum(min_threshold, sub_med)
    sub_max = np.maximum(min_threshold, sub_max)

    min_rbf = Rbf([_[0] for _ in sub_verts], [_[1] for _ in sub_verts], [_[2] for _ in sub_verts], sub_min, smooth=10, function='linear')

 
    med_rbf = Rbf([_[0] for _ in sub_verts], [_[1] for _ in sub_verts], [_[2] for _ in sub_verts], sub_med, smooth=10, function='linear')

    #print heapy.heap()
    max_rbf = Rbf([_[0] for _ in sub_verts], [_[1] for _ in sub_verts], [_[2] for _ in sub_verts], sub_max, smooth=10, function='linear')


    r_lo = max(0.2*(med_threshold - min_threshold), 0.1)
    r_hi = max(0.2*(max_threshold - med_threshold), 0.1)

    nsignal = np.zeros(signal.shape, dtype=np.float32)
    B = 1000
    low = np.zeros((B,))
    median = np.zeros((B,))
    high = np.zeros((B,))
    for i in range(0, verts.shape[0], B):
        h = min(i+B, verts.shape[0])
        sl = slice(i, h)

#        low = min_rbf(verts[sl,0].T, verts[sl, 1].T, verts[sl, 2].T)
#        median = med_rbf(verts[sl, 0].T, verts[sl, 1].T, verts[sl, 2].T)
#        high = max_rbf(verts[sl, 0].T, verts[sl, 1].T, verts[sl, 2].T)
        eval_rbf_linear(min_rbf, verts[sl,:], low)
        eval_rbf_linear(med_rbf, verts[sl,:], median)
        eval_rbf_linear(max_rbf, verts[sl,:], high)
        s = signal[sl]
        if h != i+B:
            low = low[:h-i]
            median = median[:h-i]
            high = high[:h-i]
            print low.shape, median.shape, high.shape, s.shape, nsignal[sl].shape
#        print np.mean(s), s.shape, np.mean(median), median.shape
        nsignal[sl] = np.where(s>median, 0.5*(s - median)/(np.maximum(high - median, r_hi))+0.5, 0.5-0.5*(median - s)/(np.maximum(median - low, r_lo)))
        np.clip(nsignal[sl], 0.0, 1.0, out=nsignal[sl])
        if i%10000==0:
            print i, nsignal[i], signal[i], median[0], high[0]
    #nsignal = np.maximum((nsignal - median), 0)
    high = float(np.max(nsignal))
    low = float(np.min(nsignal))
    nsignal = 255*((nsignal-low)/(high-low))

    return nsignal.astype(np.uint8)
    

def calc_aniso(pts, pt_weights):
    if len(pt_weights)==0:
        return 0.0
    mw = np.mean(pt_weights)
    if mw < 1e-12:
        return 0.0
    c = np.mean(pts*pt_weights[:,np.newaxis], axis=0)/mw
    npts = (pts - c[np.newaxis,:])#*pt_weights[:,np.newaxis]
    s = eig(np.dot(npts.T, npts*pt_weights[:,np.newaxis]))
    s = s[0]
    s.sort()
    if s[1]>0:
        mu = sqrt(s[2]/s[1])-1
    else:
        mu = 0.0
    return mu

def find_local_minima(mesh, s):
    minima = []
    A = mesh.connectivity
    for i in range(len(mesh.verts)):
        if A.indptr[i]<A.indptr[i+1]:
            min_other = np.amin(s[A.indices[A.indptr[i]:A.indptr[i+1]]])
            if s[i] < min_other:
                minima.append(i)
    return minima
        
def smooth_signal(mesh, s, delta=0.1, iterations=1):
    s = s[:, np.newaxis]
    A = mesh.connectivity
    D = mesh.degree 
    for i in range(iterations):
        s = (1-delta)*s + delta*mesh.connectivity.dot(s)/(mesh.degree+1e-12)        
    s = np.squeeze(np.asarray(s))
    return s

def connected_components_l(mesh, to_visit):
    components = []
    cc = 1
    s = set()
    A = mesh.connectivity
    while to_visit:
        s.add(iter(to_visit).next())
        cl = []
        while s:
            i = s.pop()
            to_visit.remove(i)
            cl.append(i)
            neighbours = A.indices[A.indptr[i]:A.indptr[i+1]]
            for j in neighbours:
                if j in to_visit:
                    s.add(j)
            #print cc, len(to_visit)
        cc +=1
        components.append(cl)
    return components


def connected_components(mesh, mask, labels):
    to_visit = set(i for i,v in enumerate(mask) if v>0)
    components = np.zeros(labels.shape, dtype='int')
    cc = 1
    s = set()
    A = mesh.connectivity
    while to_visit:
        s.add(iter(to_visit).next())
        while s:
            i = s.pop()
            to_visit.remove(i)
            components[i] = cc
            neighbours = A.indices[A.indptr[i]:A.indptr[i+1]]
            for j in neighbours:
                if j in to_visit:
                    s.add(j)
        cc +=1
    return components

def remove_small_cells(mesh, labels, area_threshold=0.1, in_place=False):
    if in_place:
        labels = labels
    else:
        labels = np.array(labels)
    ca, mu, cv = calculate_cell_areas_aniso(mesh, return_cv=True)
    a = list(ca[i] for i in ca if i>0)
    mean_a = np.mean(a)
    threshold_a = area_threshold * mean_a
    print len(cv)
    removed = 0
    for i in ca:
        if ca[i] < threshold_a or len(cv[i])==1:
            labels[cv[i]]=0
#                print self.vert_labels[cv[i]]
            removed += 1
    print 'removed', removed
    return labels

def split_disconnected_cells(mesh, labels, in_place=False):
    if in_place:
        labels = labels
    else:
        labels = np.array(labels)

    A = mesh.connectivity
    for i in range(A.shape[0]):
        if A.indptr[i]==A.indptr[i+1]:
            print 'isolated', i

    cv = defaultdict(set)
    for i, l in enumerate(labels):
        cv[l].add(i)

    new_cl = np.max(labels)+1
    for l in cv:
        if l>0:
            cc = connected_components_l(mesh, cv[l])
            if len(cc)>1:
                print cc
            for c in cc[1:]:
                print 'split', l, new_cl, len(c), len(cc)
                labels[c] = new_cl
                new_cl += 1
    return labels

def calculate_vertex_areas(mesh):
    n = len(mesh.verts)
    va = np.zeros((n,), dtype=np.float32)
    v_array=np.array(mesh.verts,dtype='float32') 
    tri_array=np.array(mesh.tris,dtype='i')
    tri_pts=v_array[tri_array]
    n = np.cross( tri_pts[:,1 ] - tri_pts[:,0], 
                  tri_pts[:,2 ] - tri_pts[:,0])
    ta = np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
    for t, a in zip(mesh.tris, ta):
        va[list(t)] += a/6.0
    return va

def calculate_cell_areas_aniso(mesh, labels, return_cv=False, vertex_area=None):
    ca = {}
    mu = {}
        
    cv = defaultdict(list)
    for i, l in enumerate(labels):
        cv[l].append(i)

    print '#cells', len(cv)

    verts = mesh.verts
    if vertex_area is None:
        vertex_area = calc_vertex_area(mesh)
    for l, vl in cv.iteritems():
        va = [vertex_area[i] for i in vl]
        vx = [verts[i] for i in vl]
        ca[l] = sum(va)
        mu[l] = calc_aniso(np.array(vx), np.array(va))
    if return_cv:
        return ca, mu, cv
    else:
        return ca, mu

def calculate_cell_max_z(mesh, labels):
    mz = defaultdict(float)
    for x, l in zip(mesh.verts, labels):
        mz[l] = max(mz[l], x[2])
    return mz



def test_aniso():
    pts = npr.random((10,3))
    pt_weights = npr.random((10,))
    print calc_aniso(pts, pt_weights)
    pts = np.vstack((pts, pts[:5,:]))
    pt_weights = np.hstack((pt_weights, pt_weights[:5]))
    pt_weights[:5] *= 0.5
    pt_weights[10:] *= 0.5
    print calc_aniso(pts, pt_weights)

test_aniso()
            


        
