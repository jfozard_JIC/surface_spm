

import numpy as np
cimport numpy as np
from libc.math cimport sqrt

import cython
from cython.parallel import parallel, prange

@cython.boundscheck(False)
@cython.cdivision(True)
def calculate_vertex_areas(double[:,:] verts, long[:,:] tris, float[:] areas):
    cdef int NV = verts.shape[0]
    cdef int NT = tris.shape[0]
    cdef int i, j
    cdef double[:] n = np.zeros((3,), dtype=np.float64)
    cdef double[:] nA = np.zeros((3,), dtype=np.float64)
    cdef double[:] d0 = np.zeros((3,), dtype=np.float64)
    cdef double[:] d1 = np.zeros((3,), dtype=np.float64)
    cdef float A
    cdef unsigned int i0, i1, i2

    with nogil:
        for i in range(NT):
            i0 = tris[i,0]
            i1 = tris[i,1]
            i2 = tris[i,2]
            for j in range(3):
                d0[j] = verts[i1,j] - verts[i0,j]
            for j in range(3):
                d1[j] = verts[i2,j] - verts[i0,j]
            nA[0] = d0[1]*d1[2] - d0[2]*d1[1]
            nA[1] = d0[2]*d1[0] - d0[0]*d1[2]
            nA[2] = d0[0]*d1[1] - d0[1]*d1[0]
            A = (1.0/6.0)*sqrt(nA[0]*nA[0] + nA[1]*nA[1] + nA[2]*nA[2])
            for j in range(3):
                areas[tris[i,j]] += A
    
        


@cython.boundscheck(False)
@cython.cdivision(True)
cdef void tri_lengths(double[:] v0, double[:] v1, double[:] v2, float[:] h) nogil:            
   cdef double dx, dy, dz
   dx = v0[0] - 0.5*(v1[0] + v2[0])
   dy = v0[1] - 0.5*(v1[1] + v2[1])
   dz = v0[2] - 0.5*(v1[2] + v2[2])
   h[0] = sqrt(dx*dx + dy*dy + dz*dz)*(1.0/3.0)
   dx = v1[0] - 0.5*(v0[0] + v2[0])
   dy = v1[1] - 0.5*(v0[1] + v2[1])
   dz = v1[2] - 0.5*(v0[2] + v2[2])
   h[1] = sqrt(dx*dx + dy*dy + dz*dz)*(1.0/3.0)
   dx = v2[0] - 0.5*(v1[0] + v0[0])
   dy = v2[1] - 0.5*(v1[1] + v0[1])
   dz = v2[2] - 0.5*(v1[2] + v0[2])
   h[2] = sqrt(dx*dx + dy*dy + dz*dz)*(1.0/3.0)

@cython.boundscheck(False)
cdef void add_array(int i, int j, float v, int[:] indptr, int[:] indices, float[:] data) nogil:
    cdef int k
    for k in range(indptr[i], indptr[i+1]):
        if indices[k]==j:
           data[k] +=v

@cython.boundscheck(False)
@cython.cdivision(True)
def update_edge_perims(E, double[:,:] verts, long[:,:] tris):
    cdef int Nt = tris.shape[0]
    cdef float[:] h = np.zeros((3,), dtype=np.float32)
    cdef int[:] indptr = E.indptr
    cdef int[:] indices = E.indices
    cdef float[:] data = E.data
    cdef int l, i, j, k
    with nogil:
        for l in range(Nt):
            i = tris[l,0]
            j = tris[l,1]
            k = tris[l,2]
            tri_lengths(verts[i,:], verts[j,:], verts[k,:], h)
            add_array(i, j, h[2], indptr, indices, data)
            add_array(j, i, h[2], indptr, indices, data)

            add_array(i, k, h[1], indptr, indices, data)
            add_array(k, i, h[1], indptr, indices, data)

            add_array(j, k, h[0], indptr, indices, data)
            add_array(k, j, h[0], indptr, indices, data)

	

    