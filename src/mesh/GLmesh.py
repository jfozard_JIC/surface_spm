
import numpy as np
import numpy.random as npr
import scipy.linalg as la

from algo import calc_split_tris, calculate_vertex_normals
from SPMmesh import SPMMesh

class GLMesh(SPMMesh):
    def __init__(self):
        SPMMesh.__init__(self)

    def load_ply(self, filename):
        SPMMesh.load_ply(self, filename)

        v_array=np.array(self.verts, dtype=np.float32)
        v_array=v_array-np.sum(v_array, 0)/len(v_array)
        bbox=(np.min(v_array,0),  np.max(v_array,0) )
        v_array=v_array-0.5*(bbox[0] + bbox[1])
        bbox=(np.min(v_array,0),  np.max(v_array,0) )
        
        self.v_array = v_array.astype(np.float32)
        self.bbox = bbox
        self.zoom=1.0/la.norm(bbox[1])

        self.tri_array = np.array(self.tris, dtype=np.uint32)
        self.n_array = self.vert_props['normal'].astype(np.float32)

        self.orig_vert_signal = np.array(self.vert_props['signal'], dtype=np.float32)

        print 'done matrix', self.zoom
        return self.zoom


    def generate_arrays(self):
        print 'start_arrays'

        self.orig_verts = True
        npr.seed(1)
        tris = []
        v_out = self.v_array 
        idx_out = self.tri_array
        n_out = self.n_array
        vert_labels = self.vert_props['label']
        signal = self.vert_props['signal']

        nl = np.max(vert_labels)+1
        self.label_colmap = npr.random((nl,3))
        self.label_colmap[0,:] = 0.5 #10.0/255.0
#        col_out = np.array(self.label_colmap[self.vert_labels, :], dtype=np.float32)
        nsignal = 0.1+ 0.9*(signal / float(np.max(signal)))
        nsignal = nsignal.astype(np.float32)
        col_out = np.tile(nsignal,(3,1)).T
        
        print v_out.dtype, v_out.shape, n_out.dtype, n_out.shape, 
        print col_out.dtype, col_out.shape, idx_out.dtype, idx_out.shape

        return v_out, n_out, col_out, idx_out

    def update_vertex_labels(self, newlabels):
        nl = np.max(newlabels)+1
        old_nl = self.label_colmap.shape[0]
        if nl>old_nl:
            new_colmap = np.zeros((nl, 3), dtype=np.float32)
            new_colmap[:old_nl,:] = self.label_colmap
            new_colmap[old_nl:] = npr.random((nl-old_nl, 3))
            self.label_colmap = new_colmap
        SPMMesh.update_vertex_labels(self, newlabels)

    def apply_mesh_cols_obj(self, obj, view_labels=True, colmap_labels=None, split_tris=False, draw_zero=False):
        vert_labels = self.vert_props['label']
        vert_signal = self.vert_props['signal']
        if colmap_labels == None:
            colmap = self.label_colmap
            nl = np.max(vert_labels)+1
            old_nl = self.label_colmap.shape[0]
            if nl>old_nl:
                new_colmap = np.zeros((nl, 3), dtype=np.float32)
                new_colmap[:old_nl,:] = self.label_colmap
                new_colmap[old_nl:] = npr.random((nl-old_nl, 3))
                self.label_colmap = new_colmap
                colmap = new_colmap
        elif colmap_labels == 'area':
            colmap = self.gen_area_colmap()
        elif colmap_labels == 'aniso':
            colmap = self.gen_aniso_colmap()

        if split_tris:
            new_verts, new_tris, new_labels, new_signal = calc_split_tris(self.v_array, self.tris, vert_labels, vert_signal)

            new_labels = np.array(new_labels)
            v_out = np.array(new_verts,dtype=np.float32) 
            idx_out = np.array(new_tris,dtype=np.uint32)

            n_out = calculate_vertex_normals(v_out, idx_out).astype(np.float32)

            new_signal = np.array(new_signal)

            nsignal = 0.1+ 0.9*(new_signal / float(np.max(new_signal)))
            nsignal = nsignal.astype(np.float32)

            mask = new_signal >= self.threshold
        
            col_signal = np.vstack((nsignal*mask, nsignal, nsignal*mask)).T

            if view_labels:
                col_out = np.array(colmap[new_labels, :], dtype=np.float32)
                if not draw_zero:
                    row_mask = new_labels==0        
                    col_out[row_mask, :] = col_signal[row_mask,:]
            else:
                col_out = col_signal

 #           v_out, n_out, col_out, idx_out_ = self.generate_arrays()
  
            print v_out.shape, v_out.dtype, n_out.shape, n_out.dtype, col_out.shape, col_out.dtype

            obj.vb = np.concatenate((v_out,n_out,col_out),axis=1)

            obj.elVBO.set_array(idx_out) 
            obj.elCount = len(idx_out.flatten())

#            glBindVertexArray(obj.vao)
            obj.vtVBO.bind()
            obj.vtVBO.set_array(obj.vb)
            obj.vtVBO.copy_data()
#            glBindVertexArray(0)
            obj.vtVBO.unbind()
            self.orig_verts=False
        else:

            nsignal = 0.1+ 0.9*(vert_signal / float(np.max(vert_signal)))

            if not self.orig_verts:
                v_out, n_out, col_out, idx_out = self.generate_arrays()
                obj.vb = np.concatenate((v_out,n_out,col_out),axis=1)
                obj.elVBO.set_array(idx_out) 
                obj.elCount = len(idx_out.flatten())
                self.orig_verts = True
            mask = vert_signal >= self.threshold
        
            col_signal = np.vstack((nsignal*mask, nsignal, nsignal*mask)).T

            if view_labels:
                col_out = np.array(colmap[vert_labels, :], dtype=np.float32)
                if not draw_zero:
                    row_mask = vert_labels==0        
                    col_out[row_mask, :] = col_signal[row_mask,:]
            else:
                col_out = col_signal

            obj.vb[:,6:9] = col_out
            obj.vtVBO.bind()
#            glBindVertexArray(obj.vao)
            obj.vtVBO.set_array(obj.vb)
            obj.vtVBO.copy_data()
#            glBindVertexArray(0)
            obj.vtVBO.unbind()


    
"""
    def gen_area_colmap(self):
        ca, mu = self.calculate_cell_areas_aniso()

        a = list(ca[i] for i in ca if i>0)
        min_a = min(a)
        max_a = max(a)
        range_a = max(1e-12, max_a - min_a)
        print min_a, max_a
        
        col_map = np.zeros_like(self.label_colmap)
        for i, v in ca.iteritems():
            if i>0:
                col_map[i,:] = white_blue((v - min_a)/range_a)
        return col_map


    def gen_aniso_colmap(self):
        ca, mu = self.calculate_cell_areas_aniso()

        a = list(mu[i] for i in mu if i>0)
        print a

        min_a = min(a)
        max_a = max(a)
        range_a = max(1e-12, max_a - min_a)
        print min_a, max_a
            
        col_map = np.zeros_like(self.label_colmap)
        for i, v in mu.iteritems():
            if i>0:
                col_map[i,:] = white_blue((v - min_a)/range_a)
        return col_map
"""


"""
    def make_connectivity_matrix(self):
        n = len(self.verts)
        connections = {}
        for t in self.tris:
            connections[(t[0], t[1])] = 1
            connections[(t[1], t[0])] = 1
            connections[(t[1], t[2])] = 1
            connections[(t[2], t[1])] = 1
            connections[(t[2], t[0])] = 1
            connections[(t[0], t[2])] = 1
        A = dok_matrix((n,n))
        A.update(connections)
        A = A.tocsr()
        D = A.sum(axis=1)
        self.connectivity = A
        self.degree = D
        
    def smooth_signal(self, s, delta=0.1, iterations=1):
        return smooth_signal(self, s, delta, iterations)

    def connected_components_l(self, to_visit):
        components = []
        cc = 1
        s = set()
        A = self.connectivity
        while to_visit:
            s.add(iter(to_visit).next())
            cl = []
            while s:
                i = s.pop()
                to_visit.remove(i)
                cl.append(i)
                neighbours = A.indices[A.indptr[i]:A.indptr[i+1]]
                for j in neighbours:
                    if j in to_visit:
                        s.add(j)
            #print cc, len(to_visit)
            cc +=1
            components.append(cl)
        return components


    def connected_components(self, mask):
        print "begin cc"
        to_visit = set(i for i,v in enumerate(mask) if v>0)
        components = np.zeros(self.vert_signal.shape, dtype='int')
        cc = 1
        s = set()
        A = self.connectivity
        while to_visit:
            s.add(iter(to_visit).next())
            while s:
                i = s.pop()
                to_visit.remove(i)
                components[i] = cc
                neighbours = A.indices[A.indptr[i]:A.indptr[i+1]]
                for j in neighbours:
                    if j in to_visit:
                        s.add(j)
            #print cc, len(to_visit)
            cc +=1
        return components

    def remove_small_cells(self, area_threshold=0.1):
        ca, mu, cv = self.calculate_cell_areas_aniso(return_cv=True)
        a = list(ca[i] for i in ca if i>0)
        mean_a = np.mean(a)
        threshold_a = area_threshold * mean_a
        print len(cv)
        removed = 0
        for i in ca:
            if ca[i] < threshold_a or len(cv[i])==1:
                self.vert_labels[cv[i]]=0
#                print self.vert_labels[cv[i]]
                removed += 1
        print 'removed', removed

    def split_disconnected_cells(self):

        A = self.connectivity

        for i in range(A.shape[0]):
            if A.indptr[i]==A.indptr[i+1]:
                print 'isolated', i

        cv = defaultdict(set)
        for i, l in enumerate(self.vert_labels):
            cv[l].add(i)

        new_cl = np.max(self.vert_labels)+1
        for l in cv:
            if l>0:
                cc = self.connected_components_l(cv[l])
                if len(cc)>1:
                    print cc
                for c in cc[1:]:
                    print 'split', l, new_cl, len(c), len(cc)
                    self.vert_labels[c] = new_cl
                    new_cl += 1

    def find_local_minima(self, s):
        minima = []
        A = self.connectivity
        for i in range(len(self.verts)):
#            print A.indptr[i], A.indptr[i+1], s[0, A.indices[A.indptr[i]:A.indptr[i+1]]].shape
            if A.indptr[i]<A.indptr[i+1]:
                min_other = np.amin(s[A.indices[A.indptr[i]:A.indptr[i+1]]])
                if s[i] < min_other:
                    minima.append(i)
        return minima


    def calculate_vertex_areas(self):
        n = len(self.verts)
        va = np.zeros((n,), dtype=np.float32)
        v_array=np.array(self.verts,dtype='float32') 
        tri_array=np.array(self.tris,dtype='i')
        tri_pts=v_array[tri_array]
        n = np.cross( tri_pts[:,1 ] - tri_pts[:,0], 
                   tri_pts[:,2 ] - tri_pts[:,0])
        ta = np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
        for t, a in zip(self.tris, ta):
            va[list(t)] += a/6.0
        self.vertex_area = va
        return va

    def calculate_cell_areas_aniso(self, return_cv=False):
        ca = {}
        mu = {}
        
        cv = defaultdict(list)
        for i, l in enumerate(self.vert_labels):
            cv[l].append(i)

        print '#cells', len(cv)

        for l, vl in cv.iteritems():
                va = [self.vertex_area[i] for i in vl]
                vx = [self.verts[i] for i in vl]
                ca[l] = sum(va)
                mu[l] = calc_aniso(np.array(vx), np.array(va))
        if return_cv:
            return ca, mu, cv
        else:
            return ca, mu

    
    

    def calculate_cell_max_z(self):
        mz = defaultdict(float)
        for x, l in zip(self.verts, self.vert_labels):
            mz[l] = max(mz[l], x[2])
        return mz


    def calculate_edge_lengths(self):
        try:
            A = self.connectity
        except AttributeError:
            self.make_connectivity_matrix
            A = self.connectivity
        n = len(self.verts)
        d = []
        dd = {}
        for i in range(self.n):
            for j in A.indicies[A.indptr[i]:A.indptr[i+1]]:
                l = la.norm(verts[i] - verts[j])
                dd[(i,j)] = l
                d.append(la.norm(verts[i] - verts[j]))
        self.edge_lengths = d
        return dd

    def calculate_edge_perimeters(self):
        ep = defaultdict(float)
        n = len(self.verts)
        E = dok_matrix((n,n), dtype=np.float32)
        for t0, t1, t2 in self.tris:
            v0, v1, v2 = self.verts[t0], self.verts[t1], self.verts[t2]
            h0 = la.norm(v0 - 0.5*(v1+v2))/3.0
            h1 = la.norm(v1 - 0.5*(v0+v2))/3.0
            h2 = la.norm(v2 - 0.5*(v0+v1))/3.0
            ep[(t0, t1)] += h2
            ep[(t1, t0)] += h2
            ep[(t1, t2)] += h0
            ep[(t2, t1)] += h0
            ep[(t0, t2)] += h1
            ep[(t2, t0)] += h1

        E.update(ep)
        E = E.tocsr()
        self.edge_perimeters = E
        self.edge_perimeter_sums = np.squeeze(np.asarray(E.sum(axis=1)))
        return E


    def save_ply(self, filename, state):
        data = self.data
        descr = self.descr
        vert_props = [u[0] for u in descr[0][2]]
        print 'vert_props', vert_props
        if 'state' in vert_props:
            i = vert_props.index('state')
        else:
            descr[0][2].append(('state',['int']))
            i = None
        if 'signal' in vert_props:
            j = vert_props.index('signal')
        else:
            descr[0][2].append(('signal',['float']))
            j = None


        n = []
        for el, s, v in zip(data['vertex'][0], state, self.vert_signal):
            m = list(el)
            if i:
                m[i] = s
            else:
                m.append(s)
            if j:
                m[j] = v
            else:
                m.append(v)
            n.append(tuple(m))
        data['vertex'] = (n, data['vertex'][1])
        
        write_ply2(filename, descr, data)


    def save_ply_rgb(self, filename, obj):
        
        write_ply2(filename, descr, data)
"""
            


        
