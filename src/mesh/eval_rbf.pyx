
import numpy
cimport numpy

import cython
from cython.parallel import parallel, prange
from libc.math cimport sqrt

@cython.boundscheck(False)
@cython.cdivision(True)
def eval_rbf_linear(rbf, double[:,:] v, double[:] out):
    cdef double[:] nodes = rbf.nodes
    cdef double[:,:] xi = rbf.xi
    cdef int Nv = v.shape[0]
    cdef int Nxi = xi.shape[1]
    cdef double dx, dy, dz, r
    cdef int i, j
    for i in prange(Nv, nogil=True):
        out[i] = 0.0
        for j in range(Nxi):
            dx = v[i, 0] - xi[0, j]
            dy = v[i, 1] - xi[1, j]
            dz = v[i, 2] - xi[2, j]
            r = sqrt(dx*dx + dy*dy + dz*dz)
            out[i] += nodes[j]*r

    
        



