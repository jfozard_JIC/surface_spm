

import numpy as np
import scipy.ndimage as nd
import scipy.linalg as la

from scipy.sparse import dok_matrix, csr_matrix
from scipy.spatial import cKDTree


from viridis import viridis as rainbow

from algo import *

from vertex_areas import calculate_vertex_areas as vertex_areas
from vertex_areas import update_edge_perims

from mesh import Mesh

def clip(x):
    return np.maximum(np.minimum(x, 1.0), 0.0)

class SPMMesh(Mesh):
    def __init__(self):
        Mesh.__init__(self)
        self.threshold = 0

    def make_connectivity_matrix(self):
        n = len(self.verts)
        connections = {}
        for t in self.tris:
            connections[(t[0], t[1])] = 1
            connections[(t[1], t[0])] = 1
            connections[(t[1], t[2])] = 1
            connections[(t[2], t[1])] = 1
            connections[(t[2], t[0])] = 1
            connections[(t[0], t[2])] = 1
        A = dok_matrix((n,n))
        A.update(connections)
        A = A.tocsr()
        D = A.sum(axis=1)
        self.connectivity = A
        self.degree = D
        
    def smooth_signal(self, s, delta=0.1, iterations=1):
        return smooth_signal(self, s, delta, iterations)

    def connected_components(self, mask):
        return connected_components(self, mask, self.vert_props['label'])

    def remove_small_large_cells(self, area_threshold=0.1, large_threshold=10.0):
        ca, mu, cv = self.calculate_cell_areas_aniso(return_cv=True)
        a = list(ca[i] for i in ca if i>0)
        mean_a = np.mean(a)
        threshold_a = area_threshold * mean_a
        threshold_b = large_threshold * mean_a
        print len(cv)
        removed = 0
        vert_labels = self.vert_props['label']
        for i in ca:
            if ca[i] < threshold_a or ca[i]>threshold_b or len(cv[i])==1:
                vert_labels[cv[i]]=0
#                print self.vert_labels[cv[i]]
                removed += 1
        print 'removed', removed

    def split_disconnected_cells(self):
        split_disconnected_cells(self, self.vert_props['label'], in_place=True)

    def find_local_minima(self, s):
        return find_local_minima(self, s)


    def load_ply(self, fn):

        Mesh.load_ply(self, fn)
        
        # If no signal?
        if 'signal' not in self.vert_props:
            if 'color' in self.vert_props:
                self.vert_props['signal'] = np.mean(self.vert_props['color'], axis=1)
            else:
                self.vert_props['signal'] = np.zeros((self.verts.shape[0],))
        if 'label' not in self.vert_props:
            self.vert_props['label'] = np.zeros((self.verts.shape[0],), dtype=int)

        self.make_connectivity_matrix()
        self.calculate_vertex_areas()
        self.calculate_edge_perimeters()

        print 'done matrix'

    def update_vertex_labels(self, newlabels):
        self.vert_props['label'] = newlabels

    def update_vertex_signal(self, newsignal):
        self.vert_props['signal'] = newsignal
        
    def update_threshold(self, threshold):
        self.threshold = threshold
        

    def calculate_vertex_areas(self):
        """
        n = len(self.verts)
        va = np.zeros((n,), dtype=np.float32)
        v_array=np.array(self.verts,dtype='float32') 
        tri_array=np.array(self.tris,dtype='i')
        tri_pts=v_array[tri_array]
        n = np.cross( tri_pts[:,1 ] - tri_pts[:,0], 
                   tri_pts[:,2 ] - tri_pts[:,0])
        ta = np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
        for i in range(tri_array.shape[0]):
            b = ta[i]/6.0
            va[tri_array[i,:]] += b
#            va[t[1]] += b
#            va[t[2]] += b
 
        """
        va = np.zeros((self.verts.shape[0],), dtype=np.float32)
        vertex_areas(self.verts, self.tris, va)
        self.vertex_area = va
        return self.vertex_area

    def calculate_cell_areas_aniso(self, return_cv=False):
        ca = {}
        mu = {}
        
        cv = defaultdict(list)
        for i, l in enumerate(self.vert_props['label']):
            cv[l].append(i)

        print '#cells', len(cv)

        for l, vl in cv.iteritems():
                va = [self.vertex_area[i] for i in vl]
                vx = [self.verts[i] for i in vl]
                ca[l] = sum(va)
                mu[l] = calc_aniso(np.array(vx), np.array(va))
        if return_cv:
            return ca, mu, cv
        else:
            return ca, mu

    def gen_area_colmap(self):
        ca, mu = self.calculate_cell_areas_aniso()

        a = list(ca[i] for i in ca if i>0)
        min_a = np.percentile(a, 10)
        max_a = np.percentile(a, 90)
        range_a = max(1e-12, max_a - min_a)
        print min_a, max_a
        
        col_map = np.zeros_like(self.label_colmap)
        col_map[0, :] = 0.5
        for i, v in ca.iteritems():
            if i>0:
                col_map[i,:] = rainbow(clip((v - min_a)/range_a))
        return col_map


    def gen_aniso_colmap(self):
        ca, mu = self.calculate_cell_areas_aniso()

        a = list(mu[i] for i in mu if i>0)
        print a

        min_a = np.percentile(a,10)
        max_a = np.percentile(a,90)
        range_a = max(1e-12, max_a - min_a)
        print min_a, max_a
            
        col_map = np.zeros_like(self.label_colmap)
        col_map[0, :] = 0.5
        for i, v in mu.iteritems():
            if i>0:
                col_map[i,:] = rainbow(clip((v - min_a)/range_a))
        return col_map
    
    

    def calculate_cell_max_z(self):
        mz = defaultdict(float)
        for x, l in zip(self.verts, self.vert_props['label']):
            mz[l] = max(mz[l], x[2])
        return mz


    def calculate_edge_lengths(self):
        A = self.connectity
        n = len(self.verts)
        d = []
        dd = {}
        for i in range(self.n):
            for j in A.indicies[A.indptr[i]:A.indptr[i+1]]:
                l = la.norm(verts[i] - verts[j])
                dd[(i,j)] = l
                d.append(la.norm(verts[i] - verts[j]))
        self.edge_lengths = d
        return dd

    def calculate_edge_perimeters(self):
        A = self.connectivity
#        ep = defaultdict(float)
#        verts = self.verts
#        n = len(self.verts)
        E = csr_matrix(A, dtype=np.float32)
        update_edge_perims(E, self.verts, self.tris)
        """
        E = dok_matrix((n,n))
        def norm(v):
            return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])

    
        h = np.zeros((3,))
        for t0, t1, t2 in self.tris:
            v0, v1, v2 = self.verts[t0], self.verts[t1], self.verts[t2]
            
            tri_lengths(verts[t0], verts[t1], verts[t2], h)
#
#            h0 = norm(v0 - 0.5*(v1+v2))/3.0
#            h1 = norm(v1 - 0.5*(v0+v2))/3.0
#            h2 = norm(v2 - 0.5*(v0+v1))/3.0
#            E[t0, t1] += h[2]
#            E[t1, t0] += h[2]
#            E[t1, t2] += h[0]
#            E[t2, t1] += h[0]
#            E[t0, t2] += h[1]
#            E[t2, t0] += h[1]
            ep[(t0, t1)] += h[2]
            ep[(t1, t0)] += h[2]
            ep[(t1, t2)] += h[0]
            ep[(t2, t1)] += h[0]
            ep[(t0, t2)] += h[1]
            ep[(t2, t0)] += h[1]

        E.update(ep)
        E = E.tocsr()
        """
        self.edge_perimeters = E
        self.edge_perimeter_sums = np.squeeze(np.asarray(E.sum(axis=1)))
        return E
