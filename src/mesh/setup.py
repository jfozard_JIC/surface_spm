
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext_modules=[
    Extension("vertex_areas",
              sources=["vertex_areas.pyx"],
              extra_compile_args=['-fopenmp', '-O3', '-march=native', '--fast-math'],
              libraries=["m", 'gomp'] # Unix-like specific
    ),
    Extension("vertex_normals",
              sources=["vertex_normals.pyx"],
              extra_compile_args=['-fopenmp', '-O3', '-march=native', '--fast-math'],
              libraries=["m", 'gomp'] # Unix-like specific
    ),
    Extension("eval_rbf",
              sources=["eval_rbf.pyx"],
              extra_compile_args=['-fopenmp', '-O3', '-march=native', '--fast-math'],
              libraries=["m", 'gomp'] # Unix-like specific
    ),
]

setup(
  name = "vertex_areas",
  ext_modules = cythonize(ext_modules)
)
