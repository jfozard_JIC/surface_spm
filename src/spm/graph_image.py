
""" 
Segmentation Potts model on a triangulated mesh,
which at some point could be changed to be a surface in 3D

Also think about solving simple PDEs on this mesh with 
linear basis elements

"""

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import CirclePolygon
from matplotlib.collections import PatchCollection, PolyCollection, LineCollection, EllipseCollection
from matplotlib.tri import Triangulation
import itertools
import numpy.random as npr
from PIL import Image
import scipy.ndimage as nd
from math import pi, sqrt
from scipy.sparse import dok_matrix
from collections import defaultdict

from meshSPM_ws import MeshSPM


lut = npr.randint(256, size=(10000, 3)).astype(np.uint8)

def save_png(im, fn):
    img = lut[im,:]
    j = Image.fromarray(img)
    j.save(fn)


def mesh_polygon(poly, vol_max):
    """
    Use gmsh to generate a triangulated mesh of a polygon
    """
    
    f = open("test.geo","w")
    write_gmsh_poly(poly, f)
    os.system("gmsh test.geo -2 -clmax "+str(vol_max))
    f.close()
    f = open("test.msh","r")
    

    l = f.readline
    while l:
        l = f.readline()
        if l == "$Nodes\n":
            break
    l = f.readline()
    nodes = int(l)
    verts = {}
    for i in range(nodes):
        l = f.readline().split()
        verts[int(l[0])]=np.array(map(float, l[1:3]))
    while l:
        l = f.readline()
        if l == "$Elements\n":
            break
    v_list = []
    v_map = {}
    for i,v in verts.iteritems():
        v_map[i] = len(v_list)
        v_list.append(v)

    elements = int(f.readline())
    vertex_pids = {}
    lines = []
    tris = []
    for i in range(elements):
        ll = f.readline().split()
        if ll[1]=="15": # Vertex
            vertex_pids[int(ll[4])] = int(ll[3])
        if ll[1]=="1": #line
            wid = int(ll[3])
            v0 = int(ll[-2])
            v1 = int(ll[-1])
#            lines.append([wid, v0, v1])
            lines.append((v_map[v0], v_map[v1]))
        if ll[1]=="2":
            t = tuple(map(int, ll[-3:]))
            i0, i1, i2 = t
            tris.append(tuple(map(v_map.get,t)))

    return v_list, lines, tris


def write_gmsh_poly(poly, f):

    N = len(poly)
    for i, x in enumerate(poly):
        f.write("Point("+str(i+1)+")= { %f, %f, %f };"%(x[0], x[1], 0)+"\n")
        f.write("Physical Point("+str(i)+")= {"+str(i+1)+"};\n")    

    for i in range(N):
        f.write("Line ("+str(N+i+1)+") = {"+str(i+1)+", "+str((i+1)%N+1)+"};\n")
        f.write("Physical Line ("+str(N+i)+") = {"+str(N+i+1)+"};\n")
    
    f.write("Line Loop ("+str(2*N+1)+") = {")
    start = True
    for i in range(N):
        if not start:
            f.write(", ")
            f.write(str(N+i+1))
        else:
            f.write(str(N+i+1))
        start = False
    f.write("};\n")
    f.write("Plane Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.write("Physical Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.flush()
    f.close()


def calculate_dual(mesh):
    """
    Calculate the dual of the triangulation
    """
    pass
    

class Mesh(object):
    
    def __init__(self, verts, tris, bdd):
        self.verts = verts
        self.tris = tris
        self.bdd = bdd
        self.make_connectivity_matrix()
        self.calc_areas()
        self.calculate_edge_perimeters()


    def calc_areas(self):
        vert_areas = np.zeros((self.verts.shape[0],), dtype=np.float32)
        for t in self.tris:
            v0, v1, v2 = self.verts[t[0]], self.verts[t[1]], self.verts[t[2]]
            d0 = v1 - v0
            d1 = v2 - v0
            a = 0.5*(d0[0]*d1[1] - d0[1]*d1[0])
            vert_areas[t[0]] += a/3.0
            vert_areas[t[1]] += a/3.0
            vert_areas[t[2]] += a/3.0
        self.vertex_area = vert_areas

    def calculate_edge_perimeters(self):
        A = self.connectivity
        ep = defaultdict(float)
        verts = self.verts
        n = len(self.verts)

        E = dok_matrix((n,n), dtype=np.float32)
        def norm(v):
            return sqrt(v[0]*v[0] + v[1]*v[1])

    
        for t0, t1, t2 in self.tris:
            v0, v1, v2 = self.verts[t0], self.verts[t1], self.verts[t2]
            h0 = norm(v0 - 0.5*(v1+v2))/3.0
            h1 = norm(v1 - 0.5*(v0+v2))/3.0
            h2 = norm(v2 - 0.5*(v0+v1))/3.0
            ep[(t0, t1)] += h2
            ep[(t1, t0)] += h2
            ep[(t1, t2)] += h0
            ep[(t2, t1)] += h0
            ep[(t0, t2)] += h1
            ep[(t2, t0)] += h1

        E.update(ep)
        E = E.tocsr()
        self.edge_perimeters = E
        self.edge_perimeter_sums = np.squeeze(np.asarray(E.sum(axis=1))).astype(np.float32)
        return E


    @classmethod
    def from_polygon(cls, poly, h):
        """
        Method to construct mesh from polygonal region
        """
        
        v_list, lines, tris = mesh_polygon(poly, h*h)

        m = Mesh(v_list, tris, lines)
        return m


    @classmethod
    def regular_mesh(cls, m, n, w, h):
        x, y = np.meshgrid(np.linspace(0, w, m), np.linspace(0, h, n))
        verts = np.vstack((x.ravel(), y.ravel())).T
        tris = []
        for j in range(n-1):
            for i in range(m-1):
                tris.append((j*m+i, j*m+(i+1), (j+1)*m+i))
                tris.append(((j+1)*m+i, j*m+i+1, (j+1)*m+(i+1)))
        lines = []
        print m*n-1, np.amax(tris)

        m = Mesh(verts, tris, lines)
        return m

    def make_mpl_triangulation(self):
        v = np.asarray(self.verts)
        return Triangulation(v[:,0], v[:,1], self.tris)

    def draw_mesh(self):
        fig = plt.figure()
        pl = []
        for t in self.tris:
            poly = [self.verts[i] for i in t]
            pl.append(poly)
        pc = PolyCollection(pl, facecolor=(0.9, 0.9, 0.9))
        ll = []
        for v0, v1 in self.bdd:
            ll.append((self.verts[v0], self.verts[v1]))
        lc = LineCollection(ll, edgecolor=(1.0, 0.0, 0.0), linewidth=2.0)
        
        ax = fig.gca()
        ax.add_collection(pc, autolim=True)
        ax.add_collection(lc, autolim=True)
        
#        ax.autoscale_view()

        plt.show()

    def draw_mesh_signal(self):
        t = self.make_mpl_triangulation()
        plt.figure()
        plt.tripcolor(t, self.signal, shading='gouraud')
        plt.show()

    def draw_mesh_signal_points(self):
        # Calculate crude mesh bbox for point size
        bb = np.max(self.verts, axis=0)
        r0 = sqrt(bb[0]*bb[1]/len(self.verts)/pi)
        print self.verts[:2]
        fig = plt.figure()
        ax = plt.gca()
        polys = []
#        for v in self.verts:
#            polys.append(CirclePolygon(v, r0))
        #cc = PatchCollection(polys, edgecolors='none')
        v = self.verts
        print '#points', v.shape[0]
        lines = []
        A = self.connectivity
        cA = A.tocoo(copy=False)
        for (i, j) in zip(cA.row, cA.col):
            lines.append((v[i], v[j]))
        lp = LineCollection(lines, lw=0.5, alpha=0.7, colors= (0.5,0.5,0.5))
        ax.add_collection(lp)

        cc = EllipseCollection(r0*np.ones((v.shape[0],)), r0*np.ones((v.shape[0],)), np.zeros((v.shape[0],)), units='x', 
                               linewidths = (0,), offsets=v, transOffset = ax.transData)#, transOffset=ax.transData)

        cc.set_array(self.signal.flatten())
        ax.add_collection(cc, autolim=True)


        ax.autoscale_view()
#        plt.show()

    def sample_image(self, image):
        print image.shape
        self.signal = np.squeeze(nd.map_coordinates(image, np.asarray(self.verts).T))

    def make_connectivity_matrix(self):
        n = len(self.verts)
        connections = {}
        for t in self.tris:
            connections[(t[0], t[1])] = 1
            connections[(t[1], t[0])] = 1
            connections[(t[1], t[2])] = 1
            connections[(t[2], t[1])] = 1
            connections[(t[2], t[0])] = 1
            connections[(t[0], t[2])] = 1
        A = dok_matrix((n,n), dtype=np.float32)
        A.update(connections)
        A = A.tocsr()
        D = A.sum(axis=1)
        self.connectivity = A
        self.degree = D
        
    def smooth_signal(self, s, delta=0.1, iterations=1):
        s = s[:, np.newaxis]
        for i in range(iterations):
            s = (1-delta)*s + delta*self.connectivity.dot(s)/self.degree
        return np.squeeze(np.asarray(s))

    def find_local_minima(self, s):
        minima = []
        A = self.connectivity
        for i in range(len(self.verts)):
#            print A.indptr[i], A.indptr[i+1], s[0, A.indices[A.indptr[i]:A.indptr[i+1]]].shape
            min_other = np.amin(s[A.indices[A.indptr[i]:A.indptr[i+1]]])
            if s[i] <= min_other:
                minima.append(i)
        return minima


class UniformMesh(object):
    
    def __init__(self, verts, edges, bdd):
        self.verts = verts
        self.edges = edges
        self.bdd = bdd
        self.make_connectivity_matrix()
        self.calc_areas()
        self.calculate_edge_perimeters()

    def calc_areas(self):
        vert_areas = np.ones((self.verts.shape[0],), dtype=np.float32)
        self.vertex_area = vert_areas

    def calculate_edge_perimeters(self):
        E = self.connectivity
        self.edge_perimeters = E.astype(np.float32)
        self.edge_perimeter_sums = np.squeeze(np.asarray(E.sum(axis=1))).astype(np.float32)
        return E




    @classmethod
    def regular_mesh(cls, m, n, w, h):
        x, y = np.meshgrid(np.linspace(0, w, m), np.linspace(0, h, n))
        verts = np.vstack((x.ravel(), y.ravel())).T
        edges = []
        for j in range(n-1):
            for i in range(m-1):
                edges.append((j*m+i, (j+1)*m+i))
                edges.append((j*m+i, j*m+(i+1)))
                edges.append((j*m+i, (j+1)*m+(i+1)))
                edges.append((j*m+i+1, (j+1)*m+i))
        for j in range(n-1):
            i = m-1
            edges.append((j*m+i, (j+1)*m+i))
        for i in range(m-1):
            j = n-1
            edges.append((j*m+i, j*m+(i+1)))
                

        lines = []

        m = UniformMesh(verts, edges, lines)
        return m


    @classmethod
    def square_mesh(cls, m, n, w, h):
        x, y = np.meshgrid(np.linspace(0, w, m), np.linspace(0, h, n))
        verts = np.vstack((x.ravel(), y.ravel())).T
        edges = []
        for j in range(n-1):
            for i in range(m-1):
                edges.append((j*m+i, (j+1)*m+i))
                edges.append((j*m+i, j*m+(i+1)))
        for j in range(n-1):
            i = m-1
            edges.append((j*m+i, (j+1)*m+i))
        for i in range(m-1):
            j = n-1
            edges.append((j*m+i, j*m+(i+1)))
                

        lines = []

        m = UniformMesh(verts, edges, lines)
        return m


    def draw_mesh_signal_points(self):
        # Calculate crude mesh bbox for point size
        bb = np.max(self.verts, axis=0)
        r0 = sqrt(bb[0]*bb[1]/len(self.verts)/pi)
        print self.verts[:2]
        fig = plt.figure()
        ax = plt.gca()
        polys = []
#        for v in self.verts:
#            polys.append(CirclePolygon(v, r0))
        #cc = PatchCollection(polys, edgecolors='none')
        v = self.verts
        print '#points', v.shape[0]
        lines = []
        A = self.connectivity
        cA = A.tocoo(copy=False)
        for (i, j) in zip(cA.row, cA.col):
            lines.append((v[i], v[j]))
        lp = LineCollection(lines, lw=0.5, alpha=0.7, colors= (0.5,0.5,0.5))
        ax.add_collection(lp)

        cc = EllipseCollection(r0*np.ones((v.shape[0],)), r0*np.ones((v.shape[0],)), np.zeros((v.shape[0],)), units='x', 
                               linewidths = (0,), offsets=v, transOffset = ax.transData)#, transOffset=ax.transData)

        cc.set_array(self.signal.flatten())
        ax.add_collection(cc, autolim=True)


        ax.autoscale_view()
#        plt.show()

    def sample_image(self, image):
        print image.shape
        self.signal = np.squeeze(nd.map_coordinates(image, np.asarray(self.verts).T))

    def make_connectivity_matrix(self):
        n = len(self.verts)
        connections = {}
        for t in self.edges:
            connections[(t[0], t[1])] = 1
            connections[(t[1], t[0])] = 1
        A = dok_matrix((n,n), dtype=np.float32)
        A.update(connections)
        A = A.tocsr()
        D = A.sum(axis=1).astype(np.float32)
        self.connectivity = A
        self.degree = D
        
    def smooth_signal(self, s, delta=0.1, iterations=1):
        s = s[:, np.newaxis]
        for i in range(iterations):
            s = (1-delta)*s + delta*self.connectivity.dot(s)/self.degree
        return np.squeeze(np.asarray(s))

    def find_local_minima(self, s):
        minima = []
        A = self.connectivity
        for i in range(len(self.verts)):
#            print A.indptr[i], A.indptr[i+1], s[0, A.indices[A.indptr[i]:A.indptr[i+1]]].shape
            min_other = np.amin(s[A.indices[A.indptr[i]:A.indptr[i+1]]])
            if s[i] <= min_other:
                minima.append(i)
        return minima




def main():
    im = Image.open('leaf_small.png')
    ma = np.asarray(im)
    ma = np.max(ma, axis=2)
    
    ma = np.maximum(ma - nd.median_filter(ma.astype(float), 15), 0.0).astype(np.uint8)


    Sx = ma.shape[0]-1
    Sy = ma.shape[1]-1
    Nx = 2*(Sx+1)
    Ny = 2*(Sy+1)
#    m = Mesh.from_polygon([(0, 0), (Sx, 0), (Sx, Sy), (0, Sy)], 0.8)
    m = Mesh.regular_mesh(Nx, Ny, Sx, Sy)
    m.make_connectivity_matrix()
    m.sample_image(ma)
#    m.draw_mesh_signal_points()
    print "start smooth"
#    m.signal = m.smooth_signal(m.signal, 0.1, 100)
    print "end smooth"
    print m.signal.shape, m.verts.shape
#    m.draw_mesh_signal()
#    m.draw_mesh_signal_points()
#    minima = m.find_local_minima(m.signal)
#    print len(minima)
#    plt.plot([m.verts[i][0] for i in minima], [m.verts[i][1] for i in minima], 'rx')

    s0 = np.min(m.signal)
    s1 = np.max(m.signal)
    m.signal = (255.0*(m.signal-s0)/(s1-s0)).astype(np.uint8)
    
    plt.imshow(m.signal.reshape((Ny, Nx)).T>=7)
    plt.colorbar()

    print 'array maximums', m.connectivity.max(), m.edge_perimeters.max()
    print 'array means - area, edge perimeters, degree', np.mean(m.vertex_area), m.edge_perimeters.sum()/m.edge_perimeters.nnz, np.mean(m.degree)
    plt.show()

    s = MeshSPM(m, m.signal, 7)
    print s.GetParams()
#    s.SetParams({'j01':1000.0, 'j11':2000.0, 'mu':350.0, 'nu':10.0, 'temperaturescale':350.0})
    for i in range(20):
        s.Run(500)
        save_png(s.state.reshape((Ny, Nx)).T, '/tmp/g-{}.png'.format(i))

main()
