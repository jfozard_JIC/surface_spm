import numpy as np
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
from random import randint, choice, random
import matplotlib.pyplot as plt
from cpm_step import evolve_CPM
    
def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob)    

        
class MeshSPM(object):

    def __init__(self, mesh, signal):
        self.mesh = mesh
        self.signal = signal
        self.SetPars()
        self.Initialize()

    def Initialize(self):
        self.SetTemplate()
        self.SetCellSeed()
        self.InitCellMeasurements()
        self.InitSegmentation()

    def Run(self, nsteps=None, verbose=False):
        if nsteps == None:
            nsteps = self.nsteps
        for i in range(nsteps):
            if i%10==0:
                print i, 'ncells', len(np.unique(self.state))
                self.UpdatePersistance()
            #if i%100==0:
            #    self.mesh.draw_mesh_label_points(self.state)
            #    plt.savefig('/tmp/{}.png'.format(i))
            #    plt.close()
            self.UpdateSegmentation()
            self.evolve_CPM(len(self.mesh.verts))

    def SetPars(self):
        self.nsteps = 5000
        self.J = np.array(((0.0, 1000.0), (1000.0, 2000.0)))
        self.imageinteraction = 350.0
        self.temperaturescale = 150

        neighsize = np.mean(self.mesh.connectivity.sum(axis=1))

        self.totalj = neighsize*self.J[1,1]
        self.mu = self.imageinteraction*self.totalj*0.01

        self.nu = 10.0
        
        self.estimationoffset = -2
        self.mincellarea = 27

        self.temperature = 500.0
        targetchance0 = int(self.temperaturescale*self.totalj*0.01)
        self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        while abs(self.chance0-targetchance0) and self.temperature>0.0:
            
            self.temperature=max(0.0, (self.temperature + 1.0)*targetchance0/self.chance0 - 1.0)
            self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        print "temperature", self.temperature
        self.numpixels = float(max(self.signal.shape))
        self.areaconstraint = 0.0

    def SetTemplate(self):
        """
        BackgroundSubtraction
        GaussianBlur
        StretchHistogram
        EstimateTargetCoverage3D
    
        cumsum histogram
        find minval (minimum of histogram bins)
    
        energyfunction linear function from -1 (at 0) to 0 (at threshold) to 1 (at 255)
        then make energy3D
        """        

        im = self.signal
        # Background subtraction

        im2 = self.StretchHistogram(im)
        self.tcoverage = 0.9#self.EstimateTargetCoverage(im2)

        self.totaltargetarea = int(self.tcoverage*self.numpixels)

        print 'target coverage', self.tcoverage
        
        # Calculate threshold

        h = np.bincount(im2, minlength=256)
        h = np.cumsum(h)/self.numpixels

        threshold = np.where(h < self.tcoverage)[0][-1] 
        tcoverage = h[threshold]
        print 'tcoverage', tcoverage
        print 'threshold', threshold
        self.signal = im2
        self.threshold = threshold
            
        self.energyfunction = np.zeros((256,), dtype='float')
        self.energyfunction[0:threshold+1] = np.linspace(-1, 0, threshold+1)
        self.energyfunction[threshold:] = np.linspace(0, 1, 255-threshold+1)

        self.energy = self.energyfunction[im2.flatten()].reshape(im2.shape)

    def StretchHistogram(self, im2):
        im2 = im2 - np.min(im2)
        im2 = (255.0 * im2 / np.max(im2)).astype('uint8')
        return im2
                    
    def EstimateTargetCoverage(self, im2):
        nclusters = np.zeros((256,))
        tcoverage = np.zeros((256,))
        checkpoint1 = 0
        checkpoint2 = 0
        checkpoint3 = 0

        for threshold in range(1,256):
            m = im2>=threshold
            tcoverage[threshold] = np.sum(1-m.astype(int))/self.numpixels
            nclusters[threshold] = nd.label(m, structure=nd.generate_binary_structure(2,2))[1]

            if not checkpoint1:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint1 = threshold-1
            elif not checkpoint2:
                if nclusters[threshold]>nclusters[threshold-1]:
                    checkpoint2 = threshold-1
            else:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint3 = threshold-1
                    break


        threshold = checkpoint2
        print checkpoint2, tcoverage[checkpoint2]
        return tcoverage[threshold] + self.estimationoffset*0.01

    def InitCellMeasurements(self):
        N = self.ncells
        A = self.state
        bc = np.bincount(A.flatten(), minlength=N+1)
        self._area = np.array(bc[1:])
        self._old_area = np.array(self._area)
        
    def InitSegmentation(self):
        totalarea = np.sum(self.state>0)
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        self.totalarea = totalarea     
        self.old_coverage = coverage
    
    def SetCellSeed(self):
        """
        Gaussian filter in 3d, radius 2
        Find local minima 3D
        Rename Cells 3D
        Grow the cells by 1px each
        """
        
        bsignal = self.mesh.smooth_signal(self.signal, 0.1, 10)
        minima = self.mesh.find_local_minima(bsignal)
                
        self.state = np.zeros(self.signal.shape, dtype=np.int32)

        cidx = 0
        for i in minima:
            self.state[i] = cidx
            cidx += 1

        self.ncells = cidx

    def UpdatePersistance(self):
        self._dA = self._area - self._old_area
        self._old_area = np.array(self._area)

        
    def UpdateSegmentation(self):
        #ncells = np.unique(state).shape[0]-1
        totalarea = np.sum(self.state>0)
        self.totalarea = totalarea
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        dcoverage = coverage - self.tcoverage
        dtcoverage = coverage - self.old_coverage
        if dcoverage < -0.01 and dtcoverage < 0.01:
            self.areaconstraint += 0.001
        elif dcoverage > 0.01 and dtcoverage > -0.01:
            self.areaconstraint += 0.001
        self.old_coverage = coverage

        self.kappa = int(self.totalj*self.areaconstraint)

    def evolve_CPM(self, n_steps):
        evolve_CPM(self, n_steps)
        
    def evolve_CPM_old(self, n_steps):
        bdd = 0
        acc = 0
        Nv = len(self.mesh.verts)
        A = self.mesh.connectivity
        energy = self.energy
        mincellarea = self.mincellarea
        chance0 = self.chance0
        chance1 = self.chance1
        copyprob = self.copyprob
        state = self.state
        _area = self._area
        _dA = self._dA
        mu = self.mu
        nu = self.nu
        J = self.J
        totalarea = self.totalarea
        totaltargetarea = self.totaltargetarea
        kappa = self.kappa
        for _ in range(n_steps):
            i = randint(0, Nv-1)
            nbs = A.indices[A.indptr[i]:A.indptr[i+1]]
            ni = choice(nbs)
            sigma = state[i]
            celltype = 1 if sigma>0 else 0
            nsigma = state[ni]
            ncelltype = 1 if nsigma>0 else 0
            if sigma != nsigma:
                bdd += 1
                dH = 0
                for n2i in nbs:
                    n2sigma = self.state[n2i]
                    n2celltype = 1 if n2sigma>0 else 0
                    if sigma != n2sigma:
                        dH -= J[celltype, n2celltype]
                    if nsigma != n2sigma:
                        dH += J[ncelltype, n2celltype]
                signal = energy[i]
                if sigma>0:
                    dH -= mu*signal
                    if totalarea <= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                if nsigma>0:
                    dH += mu*signal
                    if totalarea >= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                # Peristance - if the source cell is growing and
                # the target cell is shrinking, make the copy attempt
                # more favourable.
                if sigma>0 and nsigma>0:
                    if _dA[sigma-1]<0 and _dA[nsigma-1]>0:
                        dH -= nu

                # Dont allow cells to shrink to zero area
                # except through being copied over by another cell
                # or if signal under the pixel is greater than threshold

                if sigma>0 and nsigma==0:
                    if signal<0 and _area[sigma-1] <= mincellarea:
                        dH = chance0
                
                # Decide whether to accept copy attempt
                
                if dH < chance0 and (dH < chance1 or random() < copyprob[int(dH) - chance1 - 1]):
                    if sigma>0:
                        _area[sigma-1] -= 1
                        totalarea -= 1
                    
                    if nsigma>0:
                        _area[nsigma-1] += 1
                        totalarea += 1

                    state[i] = state[ni]
                    acc += 1
        self.totalarea = totalarea
        print n_steps, bdd, acc
        return acc

                        
            
