import numpy as np
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
from random import randint, choice, random
import matplotlib.pyplot as plt
from cpm_step_ws import evolve_CPM
#from cpm_step import evolve_CPM as evolve_CPM_o
import heapq



def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob)    

        
class MeshSPM(object):

    def __init__(self, mesh, signal, threshold):
        self.mesh = mesh
        self.signal = signal
        self.threshold = threshold
        self.SetPars()
        self.Initialize()

    def Initialize(self):
        self.SetTemplate()
        self.SetCellSeed()
        self.InitCellMeasurements()
        self.InitSegmentation()

    def Run(self, nsteps=None, verbose=False):
        if nsteps == None:
            nsteps = self.nsteps
        for i in range(nsteps):
            if i%10==0:
                print i, 'ncells', len(np.unique(self.state))
                self.UpdatePersistance()
#                self.mesh.draw_mesh_label_points(self.state)
#                plt.savefig('/tmp/{}.png'.format(i))
#                plt.close()
            self.UpdateSegmentation()
            self.evolve_CPM(len(self.mesh.verts))

    def SetPars(self):
        self.nsteps = 5000
        self.J = np.array(((0.0, 1000.0), (1000.0, 2000.0)))
        self.imageinteraction = 350.0
        self.persistencestrength = 10.0


        self.temperaturescale = 150

        self.neighsize = np.mean(self.mesh.edge_perimeter_sums)

        print 'neighsize', self.neighsize
        self.pixelsize = np.mean(self.mesh.vertex_area)
        print 'pixelsize', self.pixelsize

        self.kappa_scale = 0.0
        
        self.totalj = self.neighsize*self.J[1,1]
        print 'totalj', self.totalj
        self.mu = self.imageinteraction*self.totalj*0.01/self.pixelsize

        self.nu = self.persistencestrength*self.totalj*0.01
        
        self.mu_d = 0.0
        self.K_d = 30

        self.d_threshold = 100
        self.p_threshold = 255

        self.estimationoffset = -2
        self.mincellarea = 27

        self.temperature = 500.0
        targetchance0 = int(self.temperaturescale*0.01*self.totalj)
        self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)

        while abs(self.chance0-targetchance0) and self.temperature>0.0:
            
            self.temperature=max(0.0, (self.temperature + 1.0)*targetchance0/self.chance0 - 1.0)
            self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        print "temperature", self.temperature, self.chance0, self.chance1
        self.numpixels = np.sum(self.mesh.vertex_area)
        self.areaconstraint = 0.0


    def SetThreshold(self, threshold):
        self.threshold = threshold
        self.SetTemplate()

    def SetTemplate(self):
        """
        BackgroundSubtraction
        GaussianBlur
        StretchHistogram
        EstimateTargetCoverage3D
    
        cumsum histogram
        find minval (minimum of histogram bins)
    
        energyfunction linear function from -1 (at 0) to 0 (at threshold) to 1 (at 255)
        then make energy3D
        """        

        im = self.signal
        # Background subtraction

#        im2 = self.StretchHistogram(im)


        #h = np.bincount(im2, minlength=256)
        h = nd.sum(self.mesh.vertex_area, im, range(256))
        h = np.cumsum(h)/self.numpixels

        threshold = self.threshold #np.where(h < self.tcoverage)[0][-1] 
        self.tcoverage = h[threshold]
        print 'tcoverage', self.tcoverage
        print 'threshold', threshold
        self.totaltargetarea = int(self.tcoverage*self.numpixels)
        self.signal = im
        self.threshold = threshold
            
        self.energyfunction = np.zeros((256,), dtype='float')
        self.energyfunction[0:threshold+1] = np.linspace(-1, 0, threshold+1)
        self.energyfunction[threshold:] = np.linspace(0, 1, 255-threshold+1)

        self.energy = self.energyfunction[im.flatten()].reshape(im.shape)


    def SetState(self, state):
        self.state = state.astype(np.int32)
        self.ncells = np.max(self.state)
        va = self.mesh.vertex_area
        N = self.ncells
        A = self.state

        self._area = nd.sum(va, A, range(1, N)).astype(np.float32)
        self._old_area = np.array(self._area)
        self._min = nd.minimum(self.signal, A, range(1, N+1))


        self.InitSegmentation()
        self.UpdatePersistance()


    def GetParams(self):
        p = { 'j01': self.J[0][1],
              'j11': self.J[1][1],
              'mu': self.mu,
              'nu': self.nu,
              'mu_d': self.mu_d,
              'K_d': self.K_d,
              'd_threshold': self.d_threshold,
              'p_threshold': self.p_threshold,
              'temperaturescale': self.temperaturescale
              }
        return p
              

    def SetParams(self, p):

        print 'new_params', p
        recalc_temp = False
        self.J[0][1] = self.J[1][0] = p['j01']
        if 'j11' in p and p['j11']!=self.J[1][1]:
            self.J[1][1] = p['j11']
            recalc_temp = True
        if 'mu' in p:
            self.mu = p['mu']
        if 'nu' in p:
            self.nu = p['nu']
        if 'mu_d' in p:
            self.mu_d = p['mu_d']
        if 'K_d' in p:
            self.K_d = p['K_d']
        if 'd_threshold' in p:
            self.d_threshold = p['d_threshold']
        if 'p_threshold' in p:
            self.p_threshold = p['p_threshold']

        if ('temperaturescale' in p and p['temperaturescale'] != self.temperaturescale) or recalc_temp==True:
                self.totalj = self.neighsize*self.J[1,1]
                self.temperature = 500.0
                if 'temperaturescale' in p:
                    self.temperaturescale = p['temperaturescale']

                targetchance0 = int(self.temperaturescale*0.01*self.totalj)
                self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
                while abs(self.chance0-targetchance0) and self.temperature>0.0:
                    
                    self.temperature=max(0.0, (self.temperature + 1.0)*targetchance0/self.chance0 - 1.0)
                    self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
                print "temperature", self.temperature, self.chance0, self.chance1, targetchance0

        

    def StretchHistogram(self, im2):
        im2 = im2 - np.min(im2)
        im2 = (255.0 * im2 / np.max(im2)).astype('uint8')
        return im2
                    
    def EstimateTargetCoverage(self, im2):
        nclusters = np.zeros((256,))
        tcoverage = np.zeros((256,))
        checkpoint1 = 0
        checkpoint2 = 0
        checkpoint3 = 0

        for threshold in range(1,256):
            m = im2>=threshold
            tcoverage[threshold] = np.sum(1-m.astype(int))/self.numpixels
            nclusters[threshold] = nd.label(m, structure=nd.generate_binary_structure(2,2))[1]

            if not checkpoint1:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint1 = threshold-1
            elif not checkpoint2:
                if nclusters[threshold]>nclusters[threshold-1]:
                    checkpoint2 = threshold-1
            else:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint3 = threshold-1
                    break


        threshold = checkpoint2
        print checkpoint2, tcoverage[checkpoint2]
        return tcoverage[threshold] + self.estimationoffset*0.01

    def InitCellMeasurements(self):
        N = self.ncells
        A = self.state
        va = self.mesh.vertex_area
        print va.dtype
#        bc = np.bincount(A.flatten(), minlength=N+1)
#        self._area = np.array(bc[1:])
        self._area = nd.sum(va, A, range(1, N)).astype(np.float32)
        self._old_area = np.array(self._area)
        self._min = nd.minimum(self.signal, A, range(1, N+1))

        
    def InitSegmentation(self):
        totalarea = np.sum((self.state>0)*self.mesh.vertex_area)
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0)*self.mesh.vertex_area)
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        self.totalarea = totalarea     
        self.old_coverage = coverage
    
    def SetCellSeed(self):
        """
        Gaussian filter in 3d, radius 2
        Find local minima 3D
        Rename Cells 3D
        Grow the cells by 1px each
        """
        
        bsignal = self.mesh.smooth_signal(self.signal, 0.1, 10)
        minima = self.mesh.find_local_minima(bsignal)
                
        self.state = np.zeros(self.signal.shape, dtype=np.int32)

        cidx = 0
        for i in minima:
            self.state[i] = cidx
            cidx += 1

        self.ncells = cidx

    def UpdatePersistance(self):
        self._dA = self._area - self._old_area
        self._old_area = np.array(self._area)

        
    def UpdateSegmentation(self):
        #ncells = np.unique(state).shape[0]-1
        totalarea = np.sum((self.state>0)*self.mesh.vertex_area)
        self.totalarea = totalarea
        totalnoise = np.sum((np.logical_and(self.state>0, self.energy>0))*self.mesh.vertex_area)
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        dcoverage = coverage - self.tcoverage
        dtcoverage = coverage - self.old_coverage
        if dcoverage < -0.01 and dtcoverage < 0.01:
            self.areaconstraint += 0.001
        elif dcoverage > 0.01 and dtcoverage > -0.01:
            self.areaconstraint += 0.001
        self.old_coverage = coverage

        self.kappa = int(self.totalj*self.areaconstraint*self.kappa_scale)

    def evolve_CPM(self, n_steps):
#        self._area = self._area.astype(int)
#        self._dA = self._dA.astype(int)
#        evolve_CPM_o(self, n_steps)
        evolve_CPM(self, n_steps)
                        


        
            
        
