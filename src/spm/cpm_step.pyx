

import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp


from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):

    cdef int Nv = len(cpm.mesh.verts)
    cdef int[:] state = cpm.state
    cdef long[:] _area = cpm._area
    cdef long[:] _dA = cpm._dA
    cdef double[:,:] J = cpm.J
    cdef double[:] energy = cpm.energy

    cdef int[:] indices = cpm.mesh.connectivity.indices
    cdef int[:] indptr = cpm.mesh.connectivity.indptr

    cdef int bdd = 0
    cdef int acc = 0

    cdef int l, r
    cdef int di, dj
    cdef int i, j, ni, nj, sigma, nsigma, n2di, n2dj, n2i, n2j, n2sigma
    cdef int dir, dir2, nnb
    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, sumi, sumj
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double nu = cpm.nu
    cdef int mincellarea = cpm.mincellarea


    for _ in range(n_steps):
        i = pcg32_boundedrand(Nv)    
        nnb = indptr[i+1] - indptr[i]
        ni = indices[pcg32_boundedrand(nnb) + indptr[i]]
        sigma = state[i]
        celltype = 1 if sigma>0 else 0
        nsigma = state[ni]
        ncelltype = 1 if nsigma>0 else 0
        if sigma != nsigma:
            bdd += 1
            dH = 0
            for j in range(indptr[i], indptr[i+1]):
                n2i = indices[j]
                n2sigma = state[n2i]
                n2celltype = 1 if n2sigma>0 else 0
                if sigma != n2sigma:
                    dH -= J[celltype, n2celltype]
                if nsigma != n2sigma:
                    dH += J[ncelltype, n2celltype]
            signal = energy[i]
            if sigma>0:
                dH -= mu*signal
                if totalarea <= totaltargetarea:
                    dH += kappa
                else:
                    dH -= kappa

            if nsigma>0:
                dH += mu*signal
                if totalarea >= totaltargetarea:
                    dH += kappa
                else:
                    dH -= kappa

            # Peristance - if the source cell is growing and
            # the target cell is shrinking, make the copy attempt
            # more favourable.
            if sigma>0 and nsigma>0:
                if _dA[sigma-1]<0 and _dA[nsigma-1]>0:
                    dH -= nu

            # Dont allow cells to shrink to zero area
            # except through being copied over by another cell
            # or if signal under the pixel is greater than threshold

            if sigma>0 and nsigma==0:
                if signal<0 and _area[sigma-1] <= mincellarea:
                    dH = chance0
                
            # Decide whether to accept copy attempt
            if dH < chance0 and (dH < chance1 or
                                 ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 - 1]):               
                if sigma>0:
                    _area[sigma-1] -= 1
                    totalarea -= 1
                    
                if nsigma>0:
                    _area[nsigma-1] += 1
                    totalarea += 1

                state[i] = state[ni]
                acc += 1
    cpm.totalarea = totalarea           
    print n_steps, bdd, acc
    return acc
