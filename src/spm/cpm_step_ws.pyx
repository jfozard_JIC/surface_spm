

import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp


from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):

    cdef int Nv = len(cpm.mesh.verts)
    cdef int[:] state = cpm.state
    cdef float[:] _area = cpm._area
    cdef float[:] _dA = cpm._dA
    cdef unsigned char[:] signal = cpm.signal
    cdef double[:,:] J = cpm.J
    cdef double[:] energy = cpm.energy
    cdef unsigned char[:] _min = cpm._min
    cdef unsigned char d_threshold = cpm.d_threshold
    cdef unsigned char p_threshold = cpm.p_threshold
    cdef double K_d = cpm.K_d
    cdef double mu_d = cpm.mu_d

    cdef int[:] indices = cpm.mesh.edge_perimeters.indices
    cdef int[:] indptr = cpm.mesh.edge_perimeters.indptr
    cdef float[:] ep = cpm.mesh.edge_perimeters.data
    cdef float[:] ep_sums = cpm.mesh.edge_perimeter_sums

    cdef float[:] vertex_area = cpm.mesh.vertex_area
    cdef float va

    cdef int bdd = 0
    cdef int acc = 0

    cdef int l, r
    cdef int di, dj
    cdef int i, j, jj, ni, nj, sigma, nsigma, n2di, n2dj, n2i, n2j, n2sigma
    cdef int dir, dir2, nnb
    cdef int celltype, ncelltype, n2celltype
    cdef double dsignal, hypot, ishift, jshift, sumi, sumj
    cdef double dH

    cdef float s, t, n2p

    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double nu = cpm.nu
    cdef int mincellarea = cpm.mincellarea

    cdef unsigned char vsignal
    cdef double venergy

    for _ in range(n_steps):
# Randomly select target vertex
        i = pcg32_boundedrand(Nv)    
#        nnb = indptr[i+1] - indptr[i]
# Weight selection of neighbour as source vertex depending on length
        if indptr[i]==indptr[i+1]:
            continue
# If no neighbours return to selecting a target vertex
        s = ep_sums[i]*ldexp(pcg32_random(), -32)
        t = 0.0
        for jj in range(indptr[i], indptr[i+1]):
            t+=ep[jj]
            if t>=s:
                break
        else:
            print "err"
            return
#            jj = indptr[i+1] - 1
        ni = indices[jj]
        """
        ni = indices[pcg32_boundedrand(nnb) + indptr[i]]
	"""
        sigma = state[i]
        celltype = 1 if sigma>0 else 0
        nsigma = state[ni]
        ncelltype = 1 if nsigma>0 else 0
        if sigma != nsigma:
            va = vertex_area[i]
            bdd += 1
            dH = 0
            for j in range(indptr[i], indptr[i+1]):
                n2i = indices[j]
                n2p = ep[j]
                n2sigma = state[n2i]
                n2celltype = 1 if n2sigma>0 else 0
                if sigma != n2sigma:
                    dH -= J[celltype, n2celltype]*n2p
                if nsigma != n2sigma:
                    dH += J[ncelltype, n2celltype]*n2p
            vsignal = signal[i]
            venergy = energy[i]*vertex_area[i]

            if sigma>0:
                dH -= mu*venergy
                if totalarea <= totaltargetarea:
                    dH += kappa*va
                else:
                    dH -= kappa*va

            if nsigma>0:
                dH += mu*venergy
                if totalarea >= totaltargetarea:
                    dH += kappa*va
                else:
                    dH -= kappa*va


            # Peristance - if the source cell is growing and
            # the target cell is shrinking, make the copy attempt
            # more favourable
            """
            if sigma>0 and nsigma>0:
                if _dA[sigma-1]<0 and _dA[nsigma-1]>0:
                    dH -= nu


            """
            # Peristance - if the source cell is growing and
            # the target cell is shrinking, make the copy attempt
            # more favourable.
            if sigma>0 and nsigma>0:
                # Penalize copy on ridge between two cells
                dsignal = (<double> vsignal) - d_threshold - min(_min[sigma-1], _min[nsigma-1])
                if dsignal > 0.0: 
                    dH += mu_d * (dsignal / (K_d + dsignal) )
                elif vsignal < p_threshold: # persistence only below some threshold
                    if _dA[sigma-1]<0 and _dA[nsigma-1]>0:
                        dH -= nu
                    if _dA[sigma-1]>0 and _dA[nsigma-1]<0: # Not included in old cpm_step_weighted
                        dH += nu

            # Don't allow cells to shrink to zero area
            # except through being copied over by another cell
            # or if signal under the pixel is greater than threshold

            if sigma>0 and nsigma==0:
                if _area[sigma-1] <= mincellarea:
                    dH = chance0
                
            # Decide whether to accept copy attempt
            if dH < chance0 and (dH < chance1 or
                                 ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 - 1]):               

                # perform copy
                if sigma>0:
                    _area[sigma-1] -= va
                    totalarea -= va
                    if _area[sigma-1]<(1e-6*va) and nsigma>0:
                        _min[nsigma-1] = min(_min[nsigma-1], _min[sigma-1])
                    
                if nsigma>0:
                    _area[nsigma-1] += va
                    totalarea += va

                state[i] = state[ni]
                acc += 1
    cpm.totalarea = totalarea           
    print n_steps, bdd, acc
    return acc
