
""" 
Segmentation Potts model on a triangulated mesh,
which at some point could be changed to be a surface in 3D

Also think about solving simple PDEs on this mesh with 
linear basis elements

"""

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import CirclePolygon
from matplotlib.collections import PatchCollection, PolyCollection, LineCollection
from matplotlib.tri import Triangulation
import itertools
import numpy.random as npr
from PIL import Image
import scipy.ndimage as nd
from math import pi, sqrt
from scipy.sparse import dok_matrix

def mesh_polygon(poly, vol_max):
    """
    Use gmsh to generate a triangulated mesh of a polygon
    """
    
    f = open("test.geo","w")
    write_gmsh_poly(poly, f)
    os.system("gmsh test.geo -2 -clmax "+str(vol_max))
    f.close()
    f = open("test.msh","r")
    

    l = f.readline
    while l:
        l = f.readline()
        if l == "$Nodes\n":
            break
    l = f.readline()
    nodes = int(l)
    verts = {}
    for i in range(nodes):
        l = f.readline().split()
        verts[int(l[0])]=np.array(map(float, l[1:3]))
    while l:
        l = f.readline()
        if l == "$Elements\n":
            break
    v_list = []
    v_map = {}
    for i,v in verts.iteritems():
        v_map[i] = len(v_list)
        v_list.append(v)

    elements = int(f.readline())
    vertex_pids = {}
    lines = []
    tris = []
    for i in range(elements):
        ll = f.readline().split()
        if ll[1]=="15": # Vertex
            vertex_pids[int(ll[4])] = int(ll[3])
        if ll[1]=="1": #line
            wid = int(ll[3])
            v0 = int(ll[-2])
            v1 = int(ll[-1])
#            lines.append([wid, v0, v1])
            lines.append((v_map[v0], v_map[v1]))
        if ll[1]=="2":
            t = tuple(map(int, ll[-3:]))
            i0, i1, i2 = t
            tris.append(tuple(map(v_map.get,t)))

    return v_list, lines, tris


def write_gmsh_poly(poly, f):

    N = len(poly)
    for i, x in enumerate(poly):
        f.write("Point("+str(i+1)+")= { %f, %f, %f };"%(x[0], x[1], 0)+"\n")
        f.write("Physical Point("+str(i)+")= {"+str(i+1)+"};\n")    

    for i in range(N):
        f.write("Line ("+str(N+i+1)+") = {"+str(i+1)+", "+str((i+1)%N+1)+"};\n")
        f.write("Physical Line ("+str(N+i)+") = {"+str(N+i+1)+"};\n")
    
    f.write("Line Loop ("+str(2*N+1)+") = {")
    start = True
    for i in range(N):
        if not start:
            f.write(", ")
            f.write(str(N+i+1))
        else:
            f.write(str(N+i+1))
        start = False
    f.write("};\n")
    f.write("Plane Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.write("Physical Surface("+str(2*N+1)+") = {"+str(2*N+1)+"};\n")
    f.flush()
    f.close()


def calculate_dual(mesh):
    """
    Calculate the dual of the triangulation
    """
    pass
    

class Mesh(object):
    
    def __init__(self, verts, tris, bdd):
        self.verts = verts
        self.tris = tris
        self.bdd = bdd

    def calc_areas(self):
        vert_areas = np.zeros((verts.shape[0],))
        for t in self.tris:
            v0, v1, v2 = self.verts[t[0]], self.verts[t[1]], self.verts[t[2]]
            d0 = v1 - v0
            d1 = v2 - v0
            a = 0.5*(d0[0]*d1[1] - d0[1]*d1[0])
            vert_areas[t[0]] += a/3.0
            vert_areas[t[1]] += a/3.0
            vert_areas[t[2]] += a/3.0
        self.vert_areas = vert_areas

    @classmethod
    def from_polygon(cls, poly, h):
        """
        Method to construct mesh from polygonal region
        """
        
        v_list, lines, tris = mesh_polygon(poly, h*h)

        m = Mesh(v_list, tris, lines)
        return m

    def make_mpl_triangulation(self):
        v = np.asarray(self.verts)
        return Triangulation(v[:,0], v[:,1], self.tris)

    def draw_mesh(self):
        fig = plt.figure()
        pl = []
        for t in self.tris:
            poly = [self.verts[i] for i in t]
            pl.append(poly)
        pc = PolyCollection(pl, facecolor=(0.9, 0.9, 0.9))
        ll = []
        for v0, v1 in self.bdd:
            ll.append((self.verts[v0], self.verts[v1]))
        lc = LineCollection(ll, edgecolor=(1.0, 0.0, 0.0), linewidth=2.0)
        
        ax = fig.gca()
        ax.add_collection(pc, autolim=True)
        ax.add_collection(lc, autolim=True)
        
#        ax.autoscale_view()

        plt.show()

    def draw_mesh_signal(self):
        t = self.make_mpl_triangulation()
        plt.figure()
        plt.tripcolor(t, self.signal, shading='gouraud')
        plt.show()

    def draw_mesh_signal_points(self):
        # Calculate crude mesh bbox for point size
        bb = np.max(self.verts, axis=0)
        r0 = sqrt(bb[0]*bb[1]/len(self.verts)/pi)
        print self.verts[:2]
        plt.figure(1)
        fig = plt.figure()
        ax = plt.gca()
        polys = []
        for v in self.verts:
            polys.append(CirclePolygon(v, r0))
        cc = PatchCollection(polys, edgecolors='none')
        
        cc.set_array(self.signal.flatten())

        ax.add_collection(cc, autolim=True)
        ax.autoscale_view()
#        plt.show()

    def sample_image(self, image):
        print image.shape
        self.signal = nd.map_coordinates(image, np.asarray(self.verts).T)

    def make_connectivity_matrix(self):
        n = len(self.verts)
        connections = {}
        for t in self.tris:
            connections[(t[0], t[1])] = 1
            connections[(t[1], t[0])] = 1
            connections[(t[1], t[2])] = 1
            connections[(t[2], t[1])] = 1
            connections[(t[2], t[0])] = 1
            connections[(t[0], t[2])] = 1
        A = dok_matrix((n,n))
        A.update(connections)
        A = A.tocsr()
        D = A.sum(axis=1)
        self.connectivity = A
        self.degree = D
        
    def smooth_signal(self, s, delta=0.1, iterations=1):
        s = s[:, np.newaxis]
        for i in range(iterations):
            s = (1-delta)*s + delta*self.connectivity.dot(s)/self.degree
        return np.array(s.flatten())

    def find_local_minima(self, s):
        minima = []
        A = self.connectivity
        for i in range(len(self.verts)):
#            print A.indptr[i], A.indptr[i+1], s[0, A.indices[A.indptr[i]:A.indptr[i+1]]].shape
            min_other = np.amin(s[0, A.indices[A.indptr[i]:A.indptr[i+1]]])
            if s[0, i] <= min_other:
                minima.append(i)
        return minima

def main():
    im = Image.open('leaf_small.png')
    ma = np.asarray(im)
    ma = np.max(ma, axis=2)
    Sx = ma.shape[0]-1
    Sy = ma.shape[1]-1
    m = Mesh.from_polygon([(0, 0), (Sx, 0), (Sx, Sy), (0, Sy)], 0.8)
    m.make_connectivity_matrix()
    m.sample_image(ma)
#    m.draw_mesh_signal_points()
    print "start smooth"
    m.signal = m.smooth_signal(m.signal, 0.1, 100)
    print "end smooth"
    m.draw_mesh_signal_points()
    minima = m.find_local_minima(m.signal)
    print len(minima)
    plt.plot([m.verts[i][0] for i in minima], [m.verts[i][1] for i in minima], 'rx')

    plt.show()

main()
