import numpy as np
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
from random import randint, choice, random
import matplotlib.pyplot as plt
from cpm_step_weighted import evolve_CPM
import heapq

def erode_labels(mesh, labels, inplace=True):
    if not inplace:
        labels = np.array(labels)
    bg = set(i for i,v in enumerate(labels) if v==0)
    A = mesh.connectivity
    for i in range(len(labels)):
        if labels[i]>0:
            for j in A.indices[A.indptr[i]:A.indptr[i+1]]:            
                if j in bg:
                    labels[i] = 0
                    break
    return labels

def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob)    


def surface_watershed(mesh, signal, seeds, max_level=float('inf'), inplace=True):
    # On-mesh "watershed" to extend state labels to larger regions.
    # Not clear on exactly how to do this, so we'll pick a simple method
        
    # Priority queue for pixels (s[i], i) which neighbour
    # labelled pixels

    h = []

    labelled = 0
    Nv = len(mesh.verts)
    border = set()
    if not inplace:
        state = np.array(seeds)
    else:
        state = seeds
    A = mesh.connectivity
    # Loop over all pixels; for labelled pixels push unlabelled neighbours
    # into queue
    for i in range(Nv):
        if state[i]!=0:
            labelled +=1
            # Add nb vertices to queue
            for j in A.indices[A.indptr[i]:A.indptr[i+1]]:
                if j not in border:
                    heapq.heappush(h, (signal[j], j))
                    border.add(j)
    # Now pull pixel with smallest level, label with state of nb
    while h:
        l, i = heapq.heappop(h)
        border.remove(i)

        if l>max_level:
            break
        nbs = A.indices[A.indptr[i]:A.indptr[i+1]]
        n_states = state[nbs]
        n_signal = signal[nbs]
        state[i] = min((a,b) for a,b in zip(n_signal, n_states) if b!=0)[1]
        labelled +=1
#        print labelled, Nv, l, i
#        print state[i]
        # Push neighbours
        for j in A.indices[A.indptr[i]:A.indptr[i+1]]:
            if state[j]==0 and (j not in border):
                border.add(j)
                heapq.heappush(h, (signal[j], j))
    
    return state

        
class MeshSPM(object):

    def __init__(self, mesh, signal, threshold):
        self.mesh = mesh
        self.signal = signal
        self.threshold = threshold
        self.SetPars()
        self.Initialize()

    def Initialize(self):
        self.SetTemplate()
        self.SetCellSeed()
        self.InitCellMeasurements()
        self.InitSegmentation()

    def Run(self, nsteps=None, verbose=False):
        if nsteps == None:
            nsteps = self.nsteps
        for i in range(nsteps):
            if i%10==0:
                print i, 'ncells', len(np.unique(self.state))
                self.UpdatePersistance()
#                self.mesh.draw_mesh_label_points(self.state)
#                plt.savefig('/tmp/{}.png'.format(i))
#                plt.close()
            self.UpdateSegmentation()
            self.evolve_CPM(len(self.mesh.verts))

    def SetPars(self):
        self.nsteps = 5000
        self.J = np.array(((0.0, 1000.0), (1000.0, 2000.0)))
        self.imageinteraction = 35000.0
        self.temperaturescale = 150

        neighsize = np.mean(self.mesh.connectivity.sum(axis=1))

        self.pixelsize = np.mean(self.mesh.vertex_area)

        self.kappa_scale = 0.1
        
        self.totalj = neighsize*self.J[1,1]
        self.mu = self.imageinteraction

        self.nu = 100.0
        
        self.estimationoffset = -2
        self.mincellarea = 27

        self.temperature = 500.0
        targetchance0 = int(self.temperaturescale*0.01*self.totalj)
        self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        while abs(self.chance0-targetchance0) and self.temperature>0.0:
            
            self.temperature=max(0.0, (self.temperature + 1.0)*targetchance0/self.chance0 - 1.0)
            self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        print "temperature", self.temperature
        self.numpixels = np.sum(self.mesh.vertex_area)
        self.areaconstraint = 0.0


    def SetThreshold(self, threshold):
        self.threshold = threshold
        self.SetTemplate()

    def SetTemplate(self):
        """
        BackgroundSubtraction
        GaussianBlur
        StretchHistogram
        EstimateTargetCoverage3D
    
        cumsum histogram
        find minval (minimum of histogram bins)
    
        energyfunction linear function from -1 (at 0) to 0 (at threshold) to 1 (at 255)
        then make energy3D
        """        

        im = self.signal
        # Background subtraction

#        im2 = self.StretchHistogram(im)


        #h = np.bincount(im2, minlength=256)
        h = nd.sum(self.mesh.vertex_area, im, range(256))
        h = np.cumsum(h)/self.numpixels

        threshold = self.threshold #np.where(h < self.tcoverage)[0][-1] 
        self.tcoverage = h[threshold]
        print 'tcoverage', self.tcoverage
        print 'threshold', threshold
        self.totaltargetarea = int(self.tcoverage*self.numpixels)
        self.signal = im
        self.threshold = threshold
            
        self.energyfunction = np.zeros((256,), dtype='float')
        self.energyfunction[0:threshold+1] = np.linspace(-1, 0, threshold+1)
        self.energyfunction[threshold:] = np.linspace(0, 1, 255-threshold+1)

        self.energy = self.energyfunction[im.flatten()].reshape(im.shape)


    def SetState(self, state):
        self.state = state.astype(np.int32)
        self.ncells = np.max(self.state)
        va = self.mesh.vertex_area
        N = self.ncells
        A = self.state

        self._area = nd.sum(va, A, range(1, N)).astype(np.float32)
        self._old_area = np.array(self._area)

        self.InitSegmentation()
        self.UpdatePersistance()


    def StretchHistogram(self, im2):
        im2 = im2 - np.min(im2)
        im2 = (255.0 * im2 / np.max(im2)).astype('uint8')
        return im2
                    
    def EstimateTargetCoverage(self, im2):
        nclusters = np.zeros((256,))
        tcoverage = np.zeros((256,))
        checkpoint1 = 0
        checkpoint2 = 0
        checkpoint3 = 0

        for threshold in range(1,256):
            m = im2>=threshold
            tcoverage[threshold] = np.sum(1-m.astype(int))/self.numpixels
            nclusters[threshold] = nd.label(m, structure=nd.generate_binary_structure(2,2))[1]

            if not checkpoint1:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint1 = threshold-1
            elif not checkpoint2:
                if nclusters[threshold]>nclusters[threshold-1]:
                    checkpoint2 = threshold-1
            else:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint3 = threshold-1
                    break


        threshold = checkpoint2
        print checkpoint2, tcoverage[checkpoint2]
        return tcoverage[threshold] + self.estimationoffset*0.01

    def InitCellMeasurements(self):
        N = self.ncells
        A = self.state
        va = self.mesh.vertex_area
        print va.dtype
#        bc = np.bincount(A.flatten(), minlength=N+1)
#        self._area = np.array(bc[1:])
        self._area = nd.sum(va, A, range(1, N)).astype(np.float32)
        self._old_area = np.array(self._area)
        
    def InitSegmentation(self):
        totalarea = np.sum((self.state>0)*self.mesh.vertex_area)
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0)*self.mesh.vertex_area)
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        self.totalarea = totalarea     
        self.old_coverage = coverage
    
    def SetCellSeed(self):
        """
        Gaussian filter in 3d, radius 2
        Find local minima 3D
        Rename Cells 3D
        Grow the cells by 1px each
        """
        
        bsignal = self.mesh.smooth_signal(self.signal, 0.1, 10)
        minima = self.mesh.find_local_minima(bsignal)
                
        self.state = np.zeros(self.signal.shape, dtype=np.int32)

        cidx = 0
        for i in minima:
            self.state[i] = cidx
            cidx += 1

        self.ncells = cidx

    def UpdatePersistance(self):
        self._dA = self._area - self._old_area
        self._old_area = np.array(self._area)

        
    def UpdateSegmentation(self):
        #ncells = np.unique(state).shape[0]-1
        totalarea = np.sum((self.state>0)*self.mesh.vertex_area)
        self.totalarea = totalarea
        totalnoise = np.sum((np.logical_and(self.state>0, self.energy>0))*self.mesh.vertex_area)
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        dcoverage = coverage - self.tcoverage
        dtcoverage = coverage - self.old_coverage
        if dcoverage < -0.01 and dtcoverage < 0.01:
            self.areaconstraint += 0.001
        elif dcoverage > 0.01 and dtcoverage > -0.01:
            self.areaconstraint += 0.001
        self.old_coverage = coverage

        self.kappa = int(self.totalj*self.areaconstraint*self.kappa_scale)

    def evolve_CPM(self, n_steps):
        evolve_CPM(self, n_steps)
                        


        
            
        
