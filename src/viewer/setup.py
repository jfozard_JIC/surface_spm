
from setuptools import setup

from setuptools import Extension

from Cython.Build import cythonize 
import numpy as np




module1 = Extension("libintersect", sources=["libintersect.pyx", "intersect_mesh_ray.cpp"], include_dirs=['.']) 

setup(
	name = "libintersect",
	ext_modules = cythonize([ module1])
)
