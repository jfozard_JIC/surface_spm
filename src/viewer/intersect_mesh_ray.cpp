

#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Dense"

#define LARGE 1e20


typedef Eigen::Vector3f Vec3;

float ray_tri(const Vec3& o, const Vec3& d,
	      const Vec3& v0, const Vec3& v1, const Vec3& v2)
{
  Vec3 e1 = v1 - v0;
  Vec3 e2 = v2 - v0;
  Vec3 p = d.cross(e2);

  float det = e1.dot(p);
  if((-1e-12 < det) && det < 1e-12)
    return LARGE;
  //  float inv_det = 1.0/det;                                                                        
  Vec3 t = o - v0;
  float u = t.dot(p)/det;
  if(u<0.0 || u>1.0)
    return LARGE;

  Vec3 q = t.cross(e1);
  float v = d.dot(q)/det;
  if(v<0.0 || (u+v)>1.0)
    return LARGE;

  float time = e2.dot(q)/det;
  return time;

}

extern "C"
int intersect(float verts[][3], int tris[][3], int Nv, int Nt, float start[], float end[])
{
  int closest_tri = -1;
  float closest_time = LARGE;

  Vec3 o(start);
  Vec3 e(end);

  Vec3 d = e - o;

  for(int i=0; i<Nt; i++)
    {
      float t = ray_tri(o, d, 
			Eigen::Map<Vec3>(verts[tris[i][0]]),
			Eigen::Map<Vec3>(verts[tris[i][1]]),
			Eigen::Map<Vec3>(verts[tris[i][2]]));
      if(t < closest_time && ((0.0<t) && (t<1.0))) 
	{
	  closest_time = t;
	  closest_tri = i;
	}
    }
  
  if (closest_tri!=-1)
    {
      Vec3 p = o + closest_time*d;
      float d0 = (p - Eigen::Map<Vec3>(verts[tris[closest_tri][0]])).squaredNorm();
      float d1 = (p - Eigen::Map<Vec3>(verts[tris[closest_tri][1]])).squaredNorm();
      float d2 = (p - Eigen::Map<Vec3>(verts[tris[closest_tri][2]])).squaredNorm();
      if(d0 < d1)
	{
	  if(d0 < d2)
	    {
	      return tris[closest_tri][0];
	    } 
	  else 
	    {
	      return tris[closest_tri][2];
	    }
	}
      else
	{
	  if (d1 < d2)
	    {
	      return tris[closest_tri][1];
	    }
	  else
	    {
	      return tris[closest_tri][2];
	    }
	}
    }
  else
    {
      return -1;
    }
}
