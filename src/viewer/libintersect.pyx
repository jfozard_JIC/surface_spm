
cdef extern int intersect(float *verts, unsigned int *tris, int Nv, int Nt, float *start, float *end)

cpdef int intersect_mesh_ray(float[:,:] verts, unsigned int [:,:] tris, float[:] start, float[:] end):
	return intersect(&verts[0,0], &tris[0,0], verts.shape[0], tris.shape[0], &start[0], &end[0]) 

