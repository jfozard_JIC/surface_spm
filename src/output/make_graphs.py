
import sys
import numpy as np
import scipy.ndimage as nd
import matplotlib as mpl
from matplotlib.colors import colorConverter
import matplotlib.pylab as plt
import seaborn as sns

def read_map(filename):
    map = {}
    with open(filename, 'rb') as f:
        for l in f:
            i, v1, v2 = l.split()
            map[int(i)] = (float(v1), float(v2))
    return map

def make_histograms(data, output_path):
    sns.set_style('white')
    sns.set_context('talk')
    area_cutoff = (0.01, 5)
    data = data.values()
    print data
    max_area = area_cutoff[1]*np.median([_[0] for _ in data])
    min_area = area_cutoff[0]*np.median([_[0] for _ in data])
    print max_area
    area = [a for a,b in data if min_area<=a<=max_area]
    anisotropy = [b for a,b in data if min_area<=a<=max_area]
    plt.figure(figsize=(12,6))
    plt.subplot(121)
    sns.distplot(area, bins=20, kde=False, rug=False)
    plt.xlabel("area")
    plt.ylabel("freq")
    plt.subplot(122)
    sns.distplot(anisotropy, bins=20, kde=False, rug=False)
    plt.xlabel("anisotropy")
    plt.ylabel("freq")
    plt.figure(figsize=(8,8))
    plt.scatter(x=np.array(area), y=np.array(anisotropy))
    plt.xlabel("area")
    plt.ylabel("anisotropy")
    plt.xlim(xmin=0)
    plt.ylim(ymin=0)
    plt.show()

def main():
    data = read_map(sys.argv[1])
    print data
    make_histograms(data)

if __name__=="__main__":
    main()
    
