


import numpy as np
import numpy.linalg as la
from random import randint

from math import sqrt

import sys

from ply_parser import parse_ply, write_ply

col_map = {0:(10,10,10)}

def get_col(i):
    try:
        return col_map[i]
    except KeyError:
        col_map[i] = (randint(50, 255), randint(50,255), randint(50,255))
        return col_map[i]


if __name__ == '__main__':
    descr, data = parse_ply(sys.argv[1])
    cell_key = sys.argv[3]
    print descr

    del descr[0][2][3:6]

    for col in ['red', 'green', 'blue']:
        if col not in [_[0] for _ in descr[0][2]]:
            descr[0][2].append((col, ['uchar']))
    del descr[1][2][:]
    descr[1][2].append(('vertex_indices', ['list', 'uchar', 'int']))


    # Now loop through all triangles. If label is different - split into 3 

    verts = data['vertex']    
    faces = data['face']
    new_verts = []
    new_vert_labels = []
    new_faces = []
    edge_verts = {}


    Nv = len(verts)

    for f in faces:
        vidx = f['vertex_indices']
        tri_verts = [verts[i] for i in vidx]
#        print tri_verts
        for v in tri_verts:
            if type(v[cell_key])!=int:
                print '***', v[cell_key]
        tri_labels = [int(v[cell_key]) for v in tri_verts]
        tri_verts = [np.array((v['x'], v['y'], v['z'])) for v in tri_verts]
        shared_labels = list(set(tri_labels))
        N_labels = len(shared_labels)
        if N_labels > 1:
            # Need to split cell
            c = np.mean(tri_verts, axis=0)
            new_verts.extend([c for _ in shared_labels])
            new_vert_labels.extend(shared_labels)
            centre_map = dict(zip(shared_labels, range(Nv, Nv+N_labels)))
            Nv += N_labels
            for i in range(3):
                j = (i+1)%3
                k = (i+2)%3
                idx_j = vidx[j]
                idx_k = vidx[k]
                label_j = tri_labels[j]
                label_k = tri_labels[k]
                if label_j==label_k:
                    # No need to split this edge
                    new_faces.append({'vertex_indices':[idx_j, idx_k, centre_map[label_j]]})
                else:
                    # Need to split edge
                    try:
                        lj = edge_verts[(label_j, idx_j, idx_k)]
                    except KeyError:
                        ec = 0.5*(tri_verts[j] + tri_verts[k])
                        new_verts.append(ec)
                        new_vert_labels.append(label_j)
                        edge_verts[(label_j, idx_j, idx_k)] = Nv
                        lj = Nv
                        Nv += 1
                    try:
                        lk = edge_verts[(label_k, idx_k, idx_j)]
                    except KeyError:
                        ec = 0.5*(tri_verts[j] + tri_verts[k])
                        new_verts.append(ec)
                        new_vert_labels.append(label_k)
                        edge_verts[(label_k, idx_k, idx_j)] = Nv
                        lk = Nv
                        Nv += 1

                    new_faces.append({'vertex_indices':[idx_j, lj, centre_map[label_j]]})
                    new_faces.append({'vertex_indices':[idx_k, centre_map[label_k], lk]})
        else:
            new_faces.append(f)
            
    # add new vertices
    for v,l in zip(new_verts, new_vert_labels):
        nv = {'x':v[0], 'y':v[1], 'z':v[2]}
        nv[cell_key] = l
        verts.append(nv)
    
    print len(verts), vidx

    data['face']  = new_faces
    



    for v in verts:
        i = int(v[cell_key])
        r, g, b = get_col(i)
        v['red'] = r
        v['green'] = g
        v['blue'] = b
    write_ply(sys.argv[2], descr, data)
    
    
                    
                    
            
        
        
    
