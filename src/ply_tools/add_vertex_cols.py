


import numpy as np
import numpy.linalg as la
from random import randint

from math import sqrt

import sys

from ply_parser import parse_ply, write_ply

col_map = {0:(10,10,10)}

def get_col(i):
    try:
        return col_map[i]
    except KeyError:
        col_map[i] = (randint(50, 255), randint(50,255), randint(50,255))
        return col_map[i]


if __name__ == '__main__':
    descr, data = parse_ply(sys.argv[1])
    cell_key = sys.argv[3]
    print descr
    for col in ['red', 'green', 'blue']:
        if col not in [_[0] for _ in descr[0][2]]:
            descr[0][2].append((col, ['uchar']))
    del descr[1][2][:]
    descr[1][2].append(('vertex_indices', ['list', 'uchar', 'int']))
    verts = data['vertex']
    for v in verts:
        i = int(v[cell_key])
        r, g, b = get_col(i)
        v['red'] = r
        v['green'] = g
        v['blue'] = b
    write_ply(sys.argv[2], descr, data)
    
    
                    
                    
            
        
        
    
