


import math
import numpy as np
import numpy.linalg as la
import numpy.random as npr
import random

from OpenGL.GL import *
from OpenGL.arrays.vbo import *
from OpenGL.arrays import GLvoidpArray, GLintArray
from OpenGL.GL.shaders import *
from OpenGL.GL.ARB.vertex_array_object import *

from OpenGL.GLU import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtOpenGL import *

import sys
from transformations import Arcball

import csv

def read_map(filename):
    map = {}
    with open(filename, 'rb') as f:
        for l in f:
            i, v1, v2 = l.split()
            map[int(i)] = (float(v1), float(v2))
    return map

def triangulate_polygon(points, p, n):
    if abs(np.dot(n, [1,0,0]))>1e-3:
        s = np.array((1,0,0))
    else:
        s = np.array((0,1,0))

    x = np.cross(n, s)
    x = x/la.norm(x)
    y = np.cross(n, x)
    pp = [(np.dot(v,x), np.dot(v,y)) for v in points]
    return geom.ear_clip(pp)

def parse_ply(filename):
    f = open(filename, 'r')
    #
    l = f.readline()
    if l[0:3]!='ply':
        raise RuntimeError('No ply magic string at beginning of file')
    l = f.readline()
    sl = l.split()
    if sl[0]!='format' or sl[1]!='ascii':
        raise RuntimeError('File not in ascii format')
    l = f.readline()
    while l.split()[0]=='comment':
        print l[l.index(' ')+1:]
        l = f.readline()
    element_list = []
    while l.strip()!='end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0]=='property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]
        eltype_data = []
        for i in range(el[1]):
            el_data = {}
            l = f.readline()
            sl = l.split()
            buf = sl
            for prop in el[2]:
                prop_name = prop[0]
                prop_type = prop[1]
                if prop_type[0] == 'list':
                    count = int(buf[0])
                    if prop_type[2]=='float' or prop_type[2]=='double':
                        prop_data = map(float, buf[1:count+1])
                    else:
                        prop_data = map(int, buf[1:count+1])
                    buf = buf[count+1:]
                else:
                    if prop_type[0]=='float' or prop_type[0]=='double':
                        prop_data = float(buf[0])
                    else:
                        prop_data = int(buf[0])
                    buf = buf[1:]
                el_data[prop_name]=prop_data
            eltype_data.append(el_data)
        data[eltype_name] = eltype_data
    print 'done_parse'
    return data


def parse_ply2(filename):
    f = open(filename, 'r')
    #
    l = f.readline()
    if l[0:3]!='ply':
        raise RuntimeError('No ply magic string at beginning of file')
    l = f.readline()
    sl = l.split()
    if sl[0]!='format' or sl[1]!='ascii':
        raise RuntimeError('File not in ascii format')
    l = f.readline()
    while l.split()[0]=='comment':
        print l[l.index(' ')+1:]
        l = f.readline()
    element_list = []
    while l.strip()!='end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0]=='property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]
        eltype_data = []
        for i in range(el[1]):
            el_data = []
            l = f.readline()
            sl = l.split()
            buf = sl
            for prop in el[2]:
                prop_name = prop[0]
                prop_type = prop[1]
                if prop_type[0] == 'list':
                    count = int(buf[0])
                    if prop_type[2]=='float' or prop_type[2]=='double':
                        prop_data = map(float, buf[1:count+1])
                    else:
                        prop_data = map(int, buf[1:count+1])
                    buf = buf[count+1:]
                else:
                    if prop_type[0]=='float' or prop_type[0]=='double':
                        prop_data = float(buf[0])
                    else:
                        prop_data = int(buf[0])
                    buf = buf[1:]
                el_data.append(prop_data)
            eltype_data.append(tuple(el_data))
        data[eltype_name] = (eltype_data, [x[0] for x in el[2]])
    print 'done_parse'
    return data


           

class Mesh(object):
    def __init__(self):
        self.verts = [] # array of vertex positions
        self.cells = {} # cell number -> list of faces (list of indices into verts)
        self.cell_colours = {} # 
        self.cell_centroids = {}

    def load_ply2(self):
        data = parse_ply2(sys.argv[1])

        label_data = read_map(sys.argv[2])

        label_areas = dict((i, v[0]) for i,v in label_data.iteritems())

        median_area = np.median(label_areas.values())
        bad_labels = [i for i, a in label_areas.iteritems() if a>3*median_area]

        for i in bad_labels:
            del label_data[i]

        label_idx = int(sys.argv[3])
        label_data = dict((i, v[label_idx]) for i,v in label_data.iteritems())

        min_label_data = np.min(label_data.values())
        max_label_data = np.percentile(label_data.values(), 80)
        

        def cmap(x):
            if x:
                u = (min(x,max_label_data) - min_label_data)/(max_label_data - min_label_data + 1e-6)
                return np.array((0, u, 0))
            else:
                return np.array((0.1, 0.1, 0.1))
                
        NV = len(data['vertex'][0])
        NF = len(data['face'][0])
        print 'NF', NF
        verts = []
        vert_cols = []
        vert_norms = []
        vert_labels = []
        print data['vertex'][1]
        x_idx = data['vertex'][1].index('x')
        y_idx = data['vertex'][1].index('y')
        z_idx = data['vertex'][1].index('z')
        nx_idx = data['vertex'][1].index('nx')
        ny_idx = data['vertex'][1].index('ny')
        nz_idx = data['vertex'][1].index('nz')
        #r_idx = data['vertex'][1].index('red')
        #g_idx = data['vertex'][1].index('green')
        #b_idx = data['vertex'][1].index('blue')
        l_idx = data['vertex'][1].index('state')
        for v in data['vertex'][0]:
            verts.append((v[x_idx], v[y_idx], v[z_idx]))
            vert_norms.append(np.array((v[nx_idx], v[ny_idx], v[nz_idx])))

#            vert_cols.append(np.array((v[r_idx], v[g_idx], v[b_idx])))
            vert_labels.append(v[l_idx])
#            print v[l_idx], label_data.get(v[l_idx])
            col = cmap(label_data.get(v[l_idx], None))
            vert_cols.append(col)


        del data['vertex']
        print 'done_vertex'
        v_array=np.array(verts,dtype='float32')
        v_array=v_array-np.sum(v_array,0)/len(v_array)
        bbox=(np.min(v_array,0),  np.max(v_array,0) )
        v_array=v_array-0.5*(bbox[0] + bbox[1])
        bbox=(np.min(v_array,0),  np.max(v_array,0) )
        zoom=1.0/la.norm(bbox[1])
        self.verts = [np.array(v) for v in v_array]
        self.vert_norms = vert_norms
        self.vert_cols = vert_cols
        self.vert_labels = vert_labels
        for f in data['face'][0]:
            vv = f[0]
            tris = []
            for i in range(len(vv)-2):
                tris.append((vv[0], vv[i+1], vv[i+2]))

                # use the real triangulation code?
#                p = [self.verts[idx] for idx in vv]
#                n = np.cross(p[1]-p[0], p[2]-p[0])
#                tt = triangulate_polygon(p, p[0], n)
#                for t in tt:
#                    tris.append((vv[t[0]], vv[t[1]], vv[t[2]]))
            
            tritype = random.randint(1,5)# f[1]

            if tritype in self.cells:
                self.cells[tritype].extend(tris)
            else:
                self.cells[tritype]=tris
        
        del data['face']
        print 'done_face'
        return zoom





    def generate_arrays(self):
        print 'start_arrays'
        npr.seed(1)
        tris = []
        tt = []
        cell_indices = {}
        for i,c in self.cells.iteritems():
            cell_indices[i] = (len(tris), len(c)) 
            tris.extend(c)
            tt.extend([i]*len(c))
#        del self.cells
        v_out=np.array(self.verts,dtype=np.float32) 
#        del self.verts
        idx_out=np.array(tris,dtype=np.uint32)
        n_out=np.array(self.vert_norms,dtype=np.float32)
#        del self.vert_norms
        nl = np.max(self.vert_labels)
        #self.col_map=(255*npr.random((nl+1,3))).astype(np.uint8)
        #self.col_map = 10*np.ones((nl+1, 3), dtype=np.uint8)
        #self.col_map[:, 0] = np.linspace(10, 255, nl+1)
#        self.col_map[0,:] = 10
        #col_out = np.array(self.col_map[self.vert_labels, :], dtype=np.float32)/255.0


#        print self.vert_cols
        col_out = np.array(self.vert_cols, dtype=np.float32)
        #col_out += 10
        col_out /= np.max(col_out)
        print 'max col', np.max(col_out)
#        del self.vert_cols
        print 'done_arrays'
        print col_out[:10, :10]
        print v_out.shape, n_out.shape, col_out.shape, idx_out.shape
        return v_out, n_out, col_out, idx_out, cell_indices

    def update_vertex_labels(self, obj, newlabels):
        self.vert_labels = newlabels
        col_out = np.array(self.col_map[self.vert_labels, :], dtype=np.float32)/255.0
        obj.vb[:,6:9] = col_out
        obj.vtVBO.bind()
        obj.vtVBO.set_array(obj.vb)
        obj.vtVBO.copy_data()
        


def translate(x,y,z):
    a = np.eye(4)
    a[0,3]=x
    a[1,3]=y
    a[2,3]=z
    return a

def scale(s):
    a = np.eye((4))
    a[0,0]=s
    a[1,1]=s
    a[2,2]=s
    a[3,3]=1.0
    return a

def perspective(fovy, aspect, zNear, zFar):
    f = 1.0/math.tan(fovy/2.0/180*math.pi)
    return np.array(((f/aspect, 0, 0, 0), (0,f,0,0), (0,0,(zFar+zNear)/(zNear-zFar), 2*zFar*zNear/(zNear-zFar)), (0, 0, -1, 0)))

def find_list(lst, pred):
    return next((i for i,v in enumerate(lst) if pred(v)),None)

def find_list(lst, pred):
    return next((i for i,v in enumerate(lst) if pred(v)),None)

class Obj:
    pass

class GLWidget(QGLWidget):
     
    def __init__(self, parent):
        QGLWidget.__init__(self, parent)
        self.setMinimumSize(500, 500)
        self.PMatrix = np.eye(4)
        self.VMatrix = np.eye(4)
        self.transparent = []
 
    def mousePressEvent(self, event):
        self.ball.down([event.pos().x(), event.pos().y()])

    def mouseMoveEvent(self,event):
        x=event.pos().x()
        y=event.pos().y()
        self.ball.drag([x,y])
        print "move"
        self.repaint()
  
    def wheelEvent(self, event):
	factor=1.41**(-event.delta()/240.0)
	self.dist=self.dist*factor
        self.repaint()

    def mouseDoubleClickEvent(self,event):
        x=event.pos().x()
        y=event.pos().y()
#        self.pick(x,self.height-y) 

        new_labels = np.max(self.mesh.vert_labels) - np.array(self.mesh.vert_labels)
        self.mesh.update_vertex_labels(self.obj[0], new_labels)
        self.repaint()
 
    def paintGL(self):
        '''
        Drawing routine
        '''
        print "paint"
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
       # glDepthFunc(GL_NEVER)
        glPolygonMode(GL_FRONT, GL_FILL)
        self.VMatrix = translate(0, 0, -self.dist).dot(self.ball.matrix()).dot(scale(self.zoom))


        for o in self.obj:
            self.render_obj(o)

    def pick(self, x, y):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glEnable(GL_DEPTH_TEST)
        for o in self.obj:
            self.render_pick_obj(o)
        a=glReadPixelsub(x,y,1,1,GL_RGBA)
        col=a[0,0,0:3]
        idx=find_list(self.mesh.col_map, lambda x: tuple(x)==tuple(col))
        print 'P', col
        if idx:
            print 'PICK: ', idx
        self.transparent =[idx]
        self.repaint()


    def resizeGL(self, w, h):
        '''
        Resize the GL window 
        '''
        self.width=w
        self.height=h       
        glViewport(0, 0, self.width, self.height)

        self.PMatrix = perspective(40.0, float(w)/h, 0.1, 10000.0)

        self.ball.place([w/2,h/2],h/2)
    
    def initializeGL(self):
        '''
        Initialize GL
        '''
        
        # set viewing projection
        glClearColor(0.0, 0.0, 0.0, 1.0)
        

        vertex = compileShader(
            """
	    attribute vec3 position;
	    attribute vec3 color;
	    attribute vec3 normal;
            varying vec3 v_color;
            varying vec3 v_normal;
            uniform mat4 mv_matrix;
            uniform mat4 p_matrix;
	    void main() {
                vec4 eye =  mv_matrix * vec4(position,1.0);
		gl_Position = p_matrix * eye;
                v_normal = (mv_matrix * vec4(normal, 0.0)).xyz;
                v_color = color;
	    }""",
            GL_VERTEX_SHADER)

        fragment = compileShader(
            """
            const vec3 light_direction =  vec3(0., 0., -1.);
            const vec4 light_diffuse = vec4(0.5, 0.5, 0.5, 0.0);
            const vec4 light_ambient = vec4(0.5, 0.5, 0.5, 1.0);
            varying vec3 v_color;
            varying vec3 v_normal;
            uniform float alpha;
       
	    void main() {
                vec3 normal = normalize(v_normal); 
                vec4 diffuse_factor
                   = max(-dot(normal, light_direction),0.0) * light_diffuse;
                vec4 ambient_diffuse_factor
                   = diffuse_factor + light_ambient;
                gl_FragColor = ambient_diffuse_factor*vec4(v_color,alpha);
	    }""",
            GL_FRAGMENT_SHADER)
            
    



        pick_vertex = compileShader(
            """
	    attribute vec3 position;
	    attribute vec3 color;
            varying vec3 v_color;
            uniform mat4 mv_matrix;
            uniform mat4 p_matrix;
	    void main() {
                vec4 eye =  mv_matrix * vec4(position,1.0);
		gl_Position = p_matrix * eye;
                v_color = color;
	    }""",GL_VERTEX_SHADER)

        pick_fragment = compileShader(
            """
            varying vec3 v_color;
	    void main() {
                gl_FragColor = vec4(v_color, 1.0);
	    }""",GL_FRAGMENT_SHADER)

        self.shader = compileProgram(vertex,fragment)

        self.pick_shader = compileProgram(pick_vertex,pick_fragment)

        self.position_location = glGetAttribLocation( 
            self.shader, 'position' 
            )
        self.normal_location = glGetAttribLocation( 
            self.shader, 'normal' 
            )
        self.color_location = glGetAttribLocation( 
            self.shader, 'color' 
            )

        self.mv_location = glGetUniformLocation(
            self.shader, 'mv_matrix'
            )
        self.p_location = glGetUniformLocation(
            self.shader, 'p_matrix'
            )
        self.alpha_location = glGetUniformLocation(
            self.shader, 'alpha'
            )
        self.pick_position_location = glGetAttribLocation( 
            self.pick_shader, 'position' 
            )
        self.pick_color_location = glGetAttribLocation( 
            self.pick_shader, 'color' 
            )

        self.pick_mv_location = glGetUniformLocation(
            self.pick_shader, 'mv_matrix'
            )
        self.pick_p_location = glGetUniformLocation(
            self.pick_shader, 'p_matrix'
            )


        self.vStride = 9*4
        self.ball = Arcball()
        self.zoom = 1.0
        self.dist = 3.0
        self.obj=[self.load_ply()]

    def load_ply(self):

        m = Mesh()
        self.mesh = m
        self.zoom = m.load_ply2()
        v_out, n_out, col_out, idx_out, cell_indices = m.generate_arrays()


        vb=np.concatenate((v_out,n_out,col_out),axis=1)
        del v_out
        del n_out
        del col_out

        
        vao = np.array([0], dtype='uint32')
        vao[0] = glGenVertexArrays(1)

        glBindVertexArray(vao[0])
        obj=Obj()
        
        obj.vb = vb
        obj.vtVBO=VBO(vb)

        print 'made VBO', vb.shape, obj.vtVBO.shape

        obj.vtVBO.bind()
        obj.vao = vao

        glEnableVertexAttribArray( self.position_location )
        glVertexAttribPointer( 
            self.position_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO 
            )

        glEnableVertexAttribArray( self.normal_location )
        glVertexAttribPointer( 
            self.normal_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO+12 
            )			
        glEnableVertexAttribArray( self.color_location )
        glVertexAttribPointer( 
            self.color_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO+24 
            )

        glBindVertexArray( 0 )
        glDisableVertexAttribArray( self.position_location )
        glDisableVertexAttribArray( self.normal_location )
        glDisableVertexAttribArray( self.color_location )
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        obj.elVBO=VBO(idx_out, target=GL_ELEMENT_ARRAY_BUFFER)
        obj.elCount=len(idx_out.flatten())
        obj.vao = vao[0]


        pick_vao = np.array([0], dtype='uint32')
        pick_vao[0] = glGenVertexArrays(1)

        glBindVertexArray(pick_vao[0])
        

        obj.vtVBO.bind()


        glEnableVertexAttribArray( self.pick_position_location )
        glVertexAttribPointer( 
            self.position_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO 
            )


        glEnableVertexAttribArray( self.pick_color_location )
        glVertexAttribPointer( 
            self.color_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO+24 
            )

        glBindVertexArray( 0 )
        glDisableVertexAttribArray( self.position_location )
        glDisableVertexAttribArray( self.normal_location )
        glDisableVertexAttribArray( self.color_location )
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        obj.elVBO=VBO(idx_out, target=GL_ELEMENT_ARRAY_BUFFER)
        obj.elCount=len(idx_out.flatten())
        obj.pick_vao = vao[0]
        del idx_out


        obj.cell_indices = cell_indices
        self.transparent = obj.cell_indices

        print 'made obj'
        return obj

        


#    @profile
    def render_obj(self, obj):

        glEnable(GL_CULL_FACE)
        glDepthMask(True)

        glUseProgram(self.shader)
#        obj.vtVBO.bind()
        glBindVertexArray( obj.vao )
        print "copied", obj.elVBO.copied
        obj.elVBO.bind()

        glUniformMatrix4fv(self.mv_location, 1, True, self.VMatrix.astype('float32'))
        glUniformMatrix4fv(self.p_location, 1, True, self.PMatrix.astype('float32'))

        order = []
        print 'start offsets'
        for i, cr in list(obj.cell_indices.iteritems()):
#            if i in self.transparent:
                count=cr[1]*3
                offset=cr[0]*3*4
#            z = self.mesh.cell_centroids[i][2]
                order.append((count, offset))
        counts = [v[0] for v in order]
        counts2 = GLintArray.zeros(len(counts))
        offsets = [v[1] for v in order]
        offsets2 = GLvoidpArray.zeros(len(offsets))
        for i in range(len(offsets)):
            counts2[i] = counts[i]
            offsets2[i] = offsets[i]


        print 'end offsets'
        

        print 'start_draw'
        """
        glDrawElements(
                GL_TRIANGLES, obj.elCount,
                GL_UNSIGNED_INT, obj.elVBO
            )
        """
        print 'done_draw'
 
        glMultiDrawElements(
                GL_TRIANGLES, counts2,
                GL_UNSIGNED_INT, offsets2, len(counts)
            )




        obj.elVBO.unbind()
        glBindVertexArray( 0 )
        glUseProgram(0)



    def render_pick_obj(self, obj):

        glUseProgram(self.pick_shader)
#        obj.vtVBO.bind()
        glBindVertexArray( obj.pick_vao )
        obj.elVBO.bind()
        
        glUniformMatrix4fv(self.pick_mv_location, 1, True, self.VMatrix.astype('float32'))

        glUniformMatrix4fv(self.pick_p_location, 1, True, self.PMatrix.astype('float32'))

        order = []
        for i, cr in list(obj.cell_indices.iteritems()):
#            if i not in self.transparent:
                count=cr[1]*3
                offset=cr[0]*3*4
                order.append((count, offset))
        counts = [v[0] for v in order]
        offsets = [v[1] for v in order]

        counts2 = GLintArray.zeros(len(counts))
        offsets2 = GLvoidpArray.zeros(len(offsets))
        for i in range(len(offsets)):
            counts2[i] = counts[i]
            offsets2[i] = offsets[i]


#        glMultiDrawElements(
#                GL_TRIANGLES, counts,
#                GL_UNSIGNED_INT, offsets
#            )
        glMultiDrawElements(
                GL_TRIANGLES, counts2,
                GL_UNSIGNED_INT, offsets2, len(counts)
            )

        obj.elVBO.unbind()
        glBindVertexArray( 0 )
        glUseProgram(0)


    


# You don't need anything below this
class WidgetDemo(QtGui.QMainWindow):
    ''' Example class for using SpiralWidget'''
    
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        widget = GLWidget(self)    
        self.setCentralWidget(widget)


if __name__ == '__main__':
    app = QtGui.QApplication(['Python GL'])
    window = WidgetDemo()
    window.show()
    app.exec_()
                    
                    
            
        
        
    
