


import numpy as np
import numpy.linalg as la
import numpy.random as npr
import random

from math import sqrt

import sys

from collections import defaultdict

from meshSPM_weighted import MeshSPM

from numpy.linalg import svd, eig


def calc_aniso(pts, pt_weights):
    mw = np.mean(pt_weights)
    c = np.mean(pts*pt_weights[:,np.newaxis], axis=0)/mw


    npts = (pts - c[np.newaxis,:])*pt_weights[:,np.newaxis]
    s = eig(np.dot(npts.T, npts))
    s = s[0]
    s.sort()
    print s
    if s[1]>0:
        mu = sqrt(s[2]/s[1])-1
    else:
        mu = 0.0
    return mu



def parse_ply2(filename):
    f = open(filename, 'r')
    #
    l = f.readline()
    if l[0:3]!='ply':
        raise RuntimeError('No ply magic string at beginning of file')
    l = f.readline()
    sl = l.split()
    if sl[0]!='format' or sl[1]!='ascii':
        raise RuntimeError('File not in ascii format')
    l = f.readline()
    while l.split()[0]=='comment':
        print l[l.index(' ')+1:]
        l = f.readline()
    element_list = []
    while l.strip()!='end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0]=='property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]
        eltype_data = []
        for i in range(el[1]):
            el_data = []
            l = f.readline()
            sl = l.split()
            buf = sl
            for prop in el[2]:
                prop_name = prop[0]
                prop_type = prop[1]
                if prop_type[0] == 'list':
                    count = int(buf[0])
                    if prop_type[2]=='float' or prop_type[2]=='double':
                        prop_data = map(float, buf[1:count+1])
                    else:
                        prop_data = map(int, buf[1:count+1])
                    buf = buf[count+1:]
                else:
                    if prop_type[0]=='float' or prop_type[0]=='double':
                        prop_data = float(buf[0])
                    else:
                        prop_data = int(buf[0])
                    buf = buf[1:]
                el_data.append(prop_data)
            eltype_data.append(tuple(el_data))
        data[eltype_name] = (eltype_data, [x[0] for x in el[2]])
    print data['face'][-1]

    print 'done_parse'
    return element_list, data


           

class Mesh(object):
    def __init__(self):
        self.verts = [] # array of vertex positions


    def load_ply2(self):
        descr, data = parse_ply2(sys.argv[1])
        self.descr = descr
        self.data = data
        NV = len(data['vertex'][0])
        NF = len(data['face'][0])
        print 'NF', NF
        verts = []
        vert_labels = []
        print data['vertex'][1]
        x_idx = data['vertex'][1].index('x')
        y_idx = data['vertex'][1].index('y')
        z_idx = data['vertex'][1].index('z')
        s_idx = data['vertex'][1].index('state')
        for v in data['vertex'][0]:
            verts.append((v[x_idx], v[y_idx], v[z_idx]))
            vert_labels.append(v[s_idx])
        #del data['vertex']
        print 'done_vertex'
        self.verts = [np.array(v) for v in verts]
        self.vert_labels = vert_labels
        self.tris = []
        for f in data['face'][0]:
            vv = f[0]
            tris = []
            for i in range(len(vv)-2):
                tris.append((vv[0], vv[i+1], vv[i+2]))

            self.tris.extend(tris)
        self.calculate_vertex_areas()

    def calculate_vertex_areas(self):
        n = len(self.verts)
        va = np.zeros((n,), dtype=np.float32)
        v_array=np.array(self.verts,dtype='float32') 
        tri_array=np.array(self.tris,dtype='i')
        tri_pts=v_array[tri_array]
        n = np.cross( tri_pts[:,1 ] - tri_pts[:,0], 
                   tri_pts[:,2 ] - tri_pts[:,0])
        ta = np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
        for t, a in zip(self.tris, ta):
            va[list(t)] += a/6.0
        self.vertex_area = va
        return va

    def calculate_cell_areas_aniso(self, z_threshold=50):
        ca = {}
        mu = {}
        
        cv = defaultdict(list)
        for i, l in enumerate(self.vert_labels):
            cv[l].append(i)

        

        for l, vl in cv.iteritems():
                va = [self.vertex_area[i] for i in vl]
                vx = [self.verts[i] for i in vl]
                if max([x[2] for x in vx]) < z_threshold:
                    ca[l] = sum(va)
                    mu[l] = calc_aniso(np.array(vx), np.array(va))
        return ca, mu

    def calculate_cell_max_z(self):
        mz = defaultdict(float)
        for x, l in zip(self.verts, self.vert_labels):
            mz[l] = max(mz[l], x[2])
        return mz


def find_list(lst, pred):
    return next((i for i,v in enumerate(lst) if pred(v)),None)

class Obj:
    pass


if __name__ == '__main__':
    m = Mesh()
    m.load_ply2()
    
    cell_areas, cell_aniso = m.calculate_cell_areas_aniso(float(sys.argv[3]))

    with open(sys.argv[2], 'w') as f:
        for i in cell_areas:
            f.write('{} {} {}\n'.format(i, cell_areas[i], cell_aniso[i]))
    
    
    
                    
                    
            
        
        
    
