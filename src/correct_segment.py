


import math
import numpy as np
import numpy.linalg as la
import numpy.random as npr
import random

from OpenGL.GL import *
from OpenGL.arrays.vbo import *
from OpenGL.arrays import GLvoidpArray, GLintArray
from OpenGL.GL.shaders import *
from OpenGL.GL.ARB.vertex_array_object import *
from scipy.sparse import dok_matrix, csr_matrix
from scipy.spatial import cKDTree

from mesh.algo import *

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

from matplotlib.figure import Figure



from OpenGL.GLU import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtOpenGL import *

import sys
from viewer.transformations import Arcball

from collections import defaultdict

from spm.meshSPM_ws import MeshSPM
from random import random

from scipy.interpolate import Rbf

from mesh.GLmesh import GLMesh as Mesh

from mesh.algo import erode_labels

#from mesh.mesh_old import  Mesh

import ctypes

from viewer.libintersect import intersect_mesh_ray

            

def translate(x,y,z):
    a = np.eye(4)
    a[0,3]=x
    a[1,3]=y
    a[2,3]=z
    return a

def scale(s):
    a = np.eye((4))
    a[0,0]=s
    a[1,1]=s
    a[2,2]=s
    a[3,3]=1.0
    return a

def perspective(fovy, aspect, zNear, zFar):
    f = 1.0/math.tan(fovy/2.0/180*math.pi)
    return np.array(((f/aspect, 0, 0, 0), (0,f,0,0), (0,0,(zFar+zNear)/(zNear-zFar), 2*zFar*zNear/(zNear-zFar)), (0, 0, -1, 0)))

def find_list(lst, pred):
    return next((i for i,v in enumerate(lst) if pred(v)),None)

def find_list(lst, pred):
    return next((i for i,v in enumerate(lst) if pred(v)),None)

class Obj:
    pass

class GLWidget(QGLWidget):
     
    def __init__(self, parent):
        QGLWidget.__init__(self, parent)
        self.setMinimumSize(500, 500)
        self.PMatrix = np.eye(4)
        self.VMatrix = np.eye(4)
        self.view_signal = False
        self.spm = None
        self.threshold = 10
        self.paint = False
        self.selected = None
        self.cell_1 = None
        self.split_tris = False
        self.lines = False
        self.draw_gray = False

    def mousePressEvent(self, event):
        print "press"
        self.setFocus()
        if self.paint:
            p = event.pos()
            w = self.size().width()
            h = self.size().height()
            mouse_x = -1.0+2*p.x()/float(w)
            mouse_y = -1.0+2*(h - p.y())/float(h)
            print 'mouse', (mouse_x, mouse_y)
            transform2 = la.inv(self.PMatrix.dot(self.VMatrix))
            zNear = -1.0
            zFar = 1.0
            p0 = transform2.dot(np.array((mouse_x, mouse_y, zNear, 1.0)))
            p1 = transform2.dot(np.array((mouse_x, mouse_y, zFar, 1.0)))
            p0 /= p0[3]
            p1 /= p1[3]
            start = np.array(p0[:3], dtype=np.float32)
            end  = np.array(p1[:3], dtype = np.float32)
            i = intersect_mesh_ray(self.v_array, self.t_array, start, end)
            print i
            if i>=0:
                vert_signal = self.mesh.vert_props['signal']
                vert_signal[i] = 255
                self.mesh.update_vertex_signal(vert_signal)
                self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=False)
                self.repaint()
        else:
            self.ball.down([event.pos().x(), event.pos().y()])

    def mouseMoveEvent(self,event):
        if self.paint:
            p = event.pos()
            w = self.size().width()
            h = self.size().height()
            mouse_x = -1.0+2*p.x()/float(w)
            mouse_y = -1.0+2*(h - p.y())/float(h)
            print 'mouse', (mouse_x, mouse_y)
            transform2 = la.inv(self.PMatrix.dot(self.VMatrix))
            zNear = -1.0
            zFar = 1.0
            p0 = transform2.dot(np.array((mouse_x, mouse_y, zNear, 1.0)))
            p1 = transform2.dot(np.array((mouse_x, mouse_y, zFar, 1.0)))
            p0 /= p0[3]
            p1 /= p1[3]
            start = np.array(p0[:3], dtype=np.float32)
            end  = np.array(p1[:3], dtype = np.float32)
            i = intersect_mesh_ray(self.v_array, self.t_array, start, end)
            print i
            if i>=0:
                vert_signal = self.mesh.vert_props['signal']
                vert_signal[i] = 255
                self.mesh.update_vertex_signal(vert_signal)
                self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=False)
                self.repaint()
        else:
            x=event.pos().x()
            y=event.pos().y()
            self.ball.drag([x,y])
            print "move"
            self.repaint()

    def mouseDoubleClickEvent(self, event):
        p = event.pos()
        w = self.size().width()
        h = self.size().height()
        mouse_x = -1.0+2*p.x()/float(w)
        mouse_y = -1.0+2*(h - p.y())/float(h)
        print 'mouse', (mouse_x, mouse_y)
        transform2 = la.inv(self.PMatrix.dot(self.VMatrix))
        zNear = -1.0
        zFar = 1.0
        p0 = transform2.dot(np.array((mouse_x, mouse_y, zNear, 1.0)))
        p1 = transform2.dot(np.array((mouse_x, mouse_y, zFar, 1.0)))
        p0 /= p0[3]
        p1 /= p1[3]
        start = np.array(p0[:3], dtype=np.float32)
        end  = np.array(p1[:3], dtype = np.float32)
        i = intersect_mesh_ray(self.v_array, self.t_array, start, end)
        print i
        if i>=0:
            j = self.mesh.vert_props['label'][i]
            if j>0 or self.draw_gray:
                self.selected = j
                print "selected cell", j


    def changeThreshold(self, value):
        print "new threshold", value
        self.threshold = value
        self.mesh.update_threshold(value)
        self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
        self.repaint()


    def keyPressEvent(self, event):
        k = event.key()
        self.handleKeyPress(k)
    
    def handleKeyPress(self, k):
        print "key"
        if k==QtCore.Qt.Key_B:
            self.mesh.update_vertex_signal(self.mesh.smooth_signal(self.mesh.vert_props['signal'], 0.1, 1))
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()


        if k==QtCore.Qt.Key_N:
            self.mesh.update_vertex_signal(local_normalize(self.mesh, self.mesh.vert_props['signal']))
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()

        if k==QtCore.Qt.Key_R:
            self.mesh.update_vertex_signal(np.array(self.mesh.orig_vert_signal))
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()

        if k==QtCore.Qt.Key_S:
            self.spm = MeshSPM(self.mesh, self.mesh.vert_props['signal'].astype(np.uint8), self.threshold)
            self.parent().addSideBar(self.spm)
            self.mesh.update_vertex_labels(self.spm.state)
#            self.mesh.update_vertex_signal((self.spm.energy>0).astype(np.float32))
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)

            self.old_threshold = self.threshold
            self.repaint()

        if k==QtCore.Qt.Key_T:
            threshold = self.threshold
            print "Threshold", threshold
            data = (self.mesh.vert_props['signal'] < threshold).astype(int)
            labels = data #self.mesh.connected_components(data)
#            print np.max(labels), np.min(labels), np.mean(labels)
            self.mesh.update_vertex_labels(labels)
            if self.spm:
                self.spm.SetState(self.mesh.vert_props['label'])

            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()

        if k==QtCore.Qt.Key_C:
            threshold = self.threshold
            print "Connected components", threshold
            data = self.mesh.vert_props['label'] > 0
            labels = self.mesh.connected_components(data)
            print np.max(labels), np.min(labels), np.mean(labels)

            self.mesh.update_vertex_labels(labels)
            if self.spm:
                self.spm.SetState(self.mesh.vert_props['label'])

            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()

        if k==QtCore.Qt.Key_E:
            if self.spm:
                if self.old_threshold != self.threshold:
                    self.spm.SetThreshold(self.threshold)
                    self.old_threshold = self.threshold
            else:
                self.spm = MeshSPM(self.mesh, self.mesh.vert_props['signal'].astype(np.uint8), self.threshold)
                self.spm.SetState(self.mesh.vert_props['label'])
                self.spm.SetThreshold(self.threshold)
                self.old_threshold = self.threshold
            self.spm.Run(100)
            self.mesh.update_vertex_labels(self.spm.state)
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()

        if k==QtCore.Qt.Key_G:
            self.split_tris = not self.split_tris 
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()
           

        if k ==QtCore.Qt.Key_M:
            self.lines = not self.lines
            self.repaint

        if k==QtCore.Qt.Key_Z:
            erode_labels(self.mesh, self.mesh.vert_props['label'], in_place=True)
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)

            if self.spm:
                self.spm.SetState(self.mesh.vert_props['label'])

            self.repaint()



        if k==QtCore.Qt.Key_W:
            print "start watershed"
            l = surface_watershed(self.mesh, self.mesh.vert_props['signal'], self.mesh.vert_props['label'], max_level=self.threshold)
            print "end watershed"
            self.mesh.update_vertex_labels(l)
            if self.spm:
                self.spm.SetState(self.mesh.vert_props['label'])
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()            

        if k==QtCore.Qt.Key_P:
            self.paint = not self.paint
            print "paint", self.paint

        if k==QtCore.Qt.Key_Delete:
            if self.selected:
                self.mesh.vert_props['label'][self.mesh.vert_props['label']==self.selected]=0
                self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
                if self.cell_1 == self.selected:
                    self.cell_1 = None
                self.selected = None
                self.repaint()

        if k==QtCore.Qt.Key_1:
            if self.selected:
                self.cell_1 = self.selected

        if k==QtCore.Qt.Key_2:
            if self.selected and self.cell_1:
                self.mesh.vert_props['label'][self.mesh.vert_props['label']==self.selected]=self.cell_1
                self.selected = self.cell_1
                self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
                self.repaint()

        if k==QtCore.Qt.Key_Space:
            if self.view_signal:
                self.view_signal = False
            else:
                self.view_signal = True
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()

        if k==QtCore.Qt.Key_A:
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, 
                                          view_labels=not self.view_signal,
                                          colmap_labels='area',
                                          draw_zero = self.draw_gray)
            self.repaint()

        if k==QtCore.Qt.Key_I:
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, 
                                          view_labels=not self.view_signal,
                                          colmap_labels='aniso',
                                          draw_zero = self.draw_gray)


            self.repaint()

        if k==QtCore.Qt.Key_3:
            self.draw_gray = True
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, 
                                          view_labels=not self.view_signal,
                                          draw_zero = self.draw_gray)


            self.repaint()


        if k==QtCore.Qt.Key_H:
            small = QtGui.QInputDialog.getDouble(self, "Min size", 
                                                 "Smallest size to retain", 0.1, decimals=3)
            large = QtGui.QInputDialog.getDouble(self, "Max size", 
                                                 "Largest size to retain", 10.0, decimals=3)
            print small, large
            self.mesh.remove_small_large_cells(small[0], large[0])

            self.selected = None
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, 
                                          view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()


        if k==QtCore.Qt.Key_J:
            self.mesh.vert_props['label'][self.mesh.vert_props['signal']>self.threshold]=0
            self.selected = None

            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, 
                                          view_labels=not self.view_signal, draw_zero = self.draw_gray)


            self.repaint()

        if k==QtCore.Qt.Key_L:
            self.mesh.split_disconnected_cells()

            self.selected = None
            self.view_signal = False
            self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, 
                                          view_labels=not self.view_signal, draw_zero = self.draw_gray)
            self.repaint()



  
    def wheelEvent(self, event):
	factor=1.41**(-event.delta()/240.0)
	self.dist=self.dist*factor
        self.repaint()

    def save_ply(self, fn):
        self.mesh.save_ply(fn)

    def save_ply_rgb(self, fn):
        self.mesh.save_ply_rgb(fn, self.mesh.vert_props['label'])

        
    def save_shape(self, fn):
        cell_areas, cell_aniso = self.mesh.calculate_cell_areas_aniso()
        with open(fn, 'w') as f:
            for i in cell_areas:
                f.write('{} {} {}\n'.format(i, cell_areas[i], cell_aniso[i]))

    def get_data(self):
        return self.mesh.calculate_cell_areas_aniso()

    def paintGL(self):
        '''
        Drawing routine
        '''
        print "paint"
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
       # glDepthFunc(GL_NEVER)
        glPolygonMode(GL_FRONT, GL_FILL)
        self.VMatrix = translate(0, 0, -self.dist).dot(self.ball.matrix()).dot(scale(self.zoom))
        print self.VMatrix

        for o in self.obj:
            self.render_obj(o)



    def resizeGL(self, w, h):
        '''
        Resize the GL window 
        '''
        self.width=w
        self.height=h       
        glViewport(0, 0, self.width, self.height)

        self.PMatrix = perspective(40.0, float(w)/h, 0.1, 100.0)

        self.ball.place([w/2,h/2],h/2)
    
    def initializeGL(self):
        '''
        Initialize GL
        '''
        
        # set viewing projection
        glClearColor(0.0, 0.0, 0.0, 1.0)
        

        vertex = compileShader(
            """
	    attribute vec3 position;
	    attribute vec3 color;
	    attribute vec3 normal;
            varying vec3 v_color;
            varying vec3 v_normal;
            uniform mat4 mv_matrix;
            uniform mat4 p_matrix;
	    void main() {
                vec4 eye =  mv_matrix * vec4(position,1.0);
		gl_Position = p_matrix * eye;
                v_normal = (mv_matrix * vec4(normal, 0.0)).xyz;
                v_color = color;
	    }""",
            GL_VERTEX_SHADER)

        fragment = compileShader(
            """
            const vec3 light_direction =  vec3(0., 0., -1.);
            const vec4 light_diffuse = vec4(0.5, 0.5, 0.5, 0.0);
            const vec4 light_ambient = vec4(0.5, 0.5, 0.5, 1.0);
            varying vec3 v_color;
            varying vec3 v_normal;
            uniform float alpha;
       
	    void main() {
                vec3 normal = normalize(v_normal); 
                vec4 diffuse_factor
                   = max(-dot(normal, light_direction),0.0) * light_diffuse;
                vec4 ambient_diffuse_factor
                   = diffuse_factor + light_ambient;
                gl_FragColor = ambient_diffuse_factor*vec4(v_color,alpha);
	    }""",
            GL_FRAGMENT_SHADER)


        self.shader = compileProgram(vertex,fragment)

        self.position_location = glGetAttribLocation( 
            self.shader, 'position' 
            )
        self.normal_location = glGetAttribLocation( 
            self.shader, 'normal' 
            )
        self.color_location = glGetAttribLocation( 
            self.shader, 'color' 
            )

        self.mv_location = glGetUniformLocation(
            self.shader, 'mv_matrix'
            )
        self.p_location = glGetUniformLocation(
            self.shader, 'p_matrix'
            )
        self.alpha_location = glGetUniformLocation(
            self.shader, 'alpha'
            )


        self.vStride = 9*4
        self.ball = Arcball()
        self.zoom = 1.0
        self.dist = 3.0
        self.obj=[self.load_ply(sys.argv[1])]
        self.mesh.apply_mesh_cols_obj(self.obj[0], split_tris=self.split_tris, view_labels=not self.view_signal, draw_zero = self.draw_gray)            

    def load_ply(self, fn):

        m = Mesh()
        self.mesh = m
        self.zoom = m.load_ply(fn)
        v_out, n_out, col_out, idx_out = m.generate_arrays()

        self.v_array = v_out

        self.t_array = idx_out

        vb=np.concatenate((v_out,n_out,col_out),axis=1)
        del v_out
        del n_out
        del col_out

        
        vao = np.array([0], dtype='uint32')
        vao[0] = glGenVertexArrays(1)

        glBindVertexArray(vao[0])
        obj=Obj()
        
        obj.vb = vb
        obj.vtVBO=VBO(vb)

        print 'made VBO', vb.shape, obj.vtVBO.shape
        print vb[:10,:10], vb.dtype


        obj.vtVBO.bind()
        obj.vao = vao

        glEnableVertexAttribArray( self.position_location )
        glVertexAttribPointer( 
            self.position_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO 
            )

        glEnableVertexAttribArray( self.normal_location )
        glVertexAttribPointer( 
            self.normal_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO+12 
            )			
        glEnableVertexAttribArray( self.color_location )
        glVertexAttribPointer( 
            self.color_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO+24 
            )

        glBindVertexArray( 0 )
        glDisableVertexAttribArray( self.position_location )
        glDisableVertexAttribArray( self.normal_location )
        glDisableVertexAttribArray( self.color_location )

        obj.vtVBO.unbind()
#        glBindBuffer(GL_ARRAY_BUFFER, 0)



        obj.elVBO=VBO(idx_out, target=GL_ELEMENT_ARRAY_BUFFER)
        obj.elCount=len(idx_out.flatten())
        obj.vao = vao[0]

        print idx_out[:10,:], idx_out.dtype

        del idx_out

        print 'made obj'
        return obj


#    @profile
    def render_obj(self, obj):

        if self.lines:
            glDisable(GL_CULL_FACE)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        else:
            glEnable(GL_CULL_FACE)
        glDepthMask(True)

        glUseProgram(self.shader)
        #obj.vtVBO.bind()
        glBindVertexArray( obj.vao )
        print "copied", obj.elVBO.copied
        obj.elVBO.bind()

        glUniformMatrix4fv(self.mv_location, 1, True, self.VMatrix.astype('float32'))
        glUniformMatrix4fv(self.p_location, 1, True, self.PMatrix.astype('float32'))


        print 'start_draw'
        glDrawElements(
                GL_TRIANGLES, obj.elCount,
                GL_UNSIGNED_INT, obj.elVBO
            )
        print 'done_draw'



        obj.elVBO.unbind()
        #obj.vtVBO.unbind()
        glBindVertexArray( 0 )
        glUseProgram(0)

        if self.lines:
            glEnable(GL_CULL_FACE)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)



class ToolBar(QtGui.QToolBar):
    def __init__(self):
        QtGui.QToolBar.__init__(self)
        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(256)
        self.slider.setValue(10)
        
        self.addWidget(self.slider)

class SideBar(QtGui.QDockWidget):
    def __init__(self, spm):
        QtGui.QDockWidget.__init__(self)
        layout = QtGui.QGridLayout()
        params = spm.GetParams()

        w = QtGui.QWidget(self)
        self.setWidget(w)
        w.setLayout(layout)
        self.wl = {}
        self.spm = spm

        i = 0
        for p, v in params.iteritems():
            if type(v)==int:
                sb = QtGui.QSpinBox()
                sb.setMinimum(0)
                sb.setMaximum(255 if v<256 else 10*v)
            else:
                sb = QtGui.QDoubleSpinBox()
                sb.setMinimum(0.01)
                sb.setMaximum(max(10*v, 100.0))
            sb.setValue(v)
            layout.addWidget(QtGui.QLabel(p), i, 0)
            layout.addWidget(sb, i, 1)
            sb.valueChanged.connect(self.set_values)
            self.wl[p] = sb
            i += 1

    def set_values(self, val):
        p = dict( (p, w.value()) for p, w in self.wl.iteritems())
        self.spm.SetParams(p)

# Fix this up!

class HistogramCanvas(QtGui.QWidget):
    def __init__(self, data, parent=None):
        QtGui.QWidget.__init__(self, parent)
        
        width = 5
        height = 4
        dpi = 100

        self.fig = Figure(figsize=(width, height), dpi=dpi)

        self.canvas = FigureCanvas(self.fig)
        self.toolbar = NavigationToolbar(self.canvas, self)

        self.axes = self.fig.add_subplot(111)
        self.axes.hold(False)

        self.axes.hist(data, 20)

        """
        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        """

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout)
    
        

class WidgetDemo(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.tbar = ToolBar()
        self.addToolBar(self.tbar)
        self.widget = GLWidget(self)    
        self.setCentralWidget(self.widget)
        self.tbar.slider.valueChanged.connect(self.widget.changeThreshold)
        #self.addDockWidget(QtCore.Qt.DockWidgetArea(2),SideBar())

        self.initMenu()

#        self.widget.setFocus()

    def addSideBar(self, spm):
        self.addDockWidget(QtCore.Qt.DockWidgetArea(2),SideBar(spm))

    def keyPressEvent(self, event):
        k = event.key()
        self.widget.handleKeyPress(k)


    def initMenu(self):

        loadPlyAction = QtGui.QAction("Load PLY", self)
        loadPlyAction.triggered.connect(self.load_ply)

        savePlyAction = QtGui.QAction("Save PLY", self)
        savePlyAction.triggered.connect(self.save_ply)

        savePlyRGBAction = QtGui.QAction("Save PLY RGB", self)
        savePlyRGBAction.triggered.connect(self.save_ply_rgb)


        saveShapeAction = QtGui.QAction("Save Shape", self)
        saveShapeAction.triggered.connect(self.save_shape)

#        drawHistogramAction = QtGui.QAction("Draw Histogram", self)
#        drawHistogramAction.triggered.connect(self.drawHistogram)
        
        exitAction = QtGui.QAction('Quit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(self.close)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(loadPlyAction)
        fileMenu.addAction(savePlyAction)
        fileMenu.addAction(saveShapeAction)
        fileMenu.addAction(exitAction)

        drawAreaHistogramAction = QtGui.QAction("Area", self)
        drawAreaHistogramAction.triggered.connect(self.draw_area_histogram)

        drawAnisoHistogramAction = QtGui.QAction("Anisotropy", self)
        drawAnisoHistogramAction.triggered.connect(self.draw_aniso_histogram)

        analysisMenu = menubar.addMenu('&Analysis')
        analysisMenu.addAction(drawAreaHistogramAction)
        analysisMenu.addAction(drawAnisoHistogramAction)



    def load_ply(self):
        #filename = QtGui.QFileDialog.getOpenFileName(self,
        #                                             'OPEN Mesh',
        #                                            QtCore.QDir.currentPath(),
        #                                             'Ply Mesh (*.ply);;All files (*.*)')
        #print filename
        #self.draw_histogram()
        pass

    def save_ply(self):
        filename = QtGui.QFileDialog.getSaveFileName(self,
                                                     'Save Mesh',
                                                    QtCore.QDir.currentPath(),
                                                     'Ply Mesh (*.ply);;All files (*.*)')
        self.widget.save_ply(filename)



    def save_ply_rgb(self):
        filename = QtGui.QFileDialog.getSaveFileName(self,
                                                     'Save Mesh RGB',
                                                    QtCore.QDir.currentPath(),
                                                     'Ply Mesh (*.ply);;All files (*.*)')
        self.widget.save_ply_rgb(filename)



    def save_shape(self):
        filename = QtGui.QFileDialog.getSaveFileName(self,
                                                     'Save Shape Data',
                                                    QtCore.QDir.currentPath(),
                                                     'CSV (*.csv);;All files (*.*)')

        self.widget.save_shape(filename)

    def draw_area_histogram(self):
        data = [ v for i, v in (self.widget.get_data()[0]).iteritems()] # if i>0]
        mean_area = np.mean(data[1:])
        ncells = np.sum(data[1:] > 1e-6*mean_area)
        adjusted_ncells = np.sum(data[1:] > 0.1*mean_area)
        total_area = np.sum(data)
        print "Area check", total_area, np.sum(self.widget.mesh.vertex_area)
        
        print "Area summary"
        print "Total NCells, Adjusted NCells, Total Area, Mean Area, Zero Area, Estimated NCells, Estimated NCells (average)"
        print ncells, adjusted_ncells, np.sum(data), mean_area, \
            data[0], adjusted_ncells + data[0]/mean_area,  total_area/mean_area, ncells + data[0]/mean_area

        self.area_hist = HistogramCanvas(data[1:])
        self.area_hist.show()

    def draw_aniso_histogram(self):
        data = [ v for i, v in (self.widget.get_data()[1]).iteritems() if i>0]
        
        print "Anisotropy summary"
        print "Mean, Median"
        print np.mean(data), np.median(data)
        self.aniso_hist = HistogramCanvas(data)
        self.aniso_hist.show()


if __name__ == '__main__':
    app = QtGui.QApplication(['Surface SPM'])
    window = WidgetDemo()
    window.show()
    app.exec_()


                    
                    
            
        
        
    
