


import math
import numpy as np
import numpy.linalg as la
import numpy.random as npr
import random

from OpenGL.GL import *
from OpenGL.arrays.vbo import *
from OpenGL.arrays import GLvoidpArray, GLintArray
from OpenGL.GL.shaders import *
from OpenGL.GL.ARB.vertex_array_object import *
from scipy.sparse import dok_matrix, csr_matrix
from scipy.spatial import cKDTree


from OpenGL.GLU import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtOpenGL import *

import sys
from transformations import Arcball

from collections import defaultdict

from meshSPM_weighted import MeshSPM, surface_watershed, erode_labels
from random import random

from scipy.interpolate import Rbf



def calculate_vertex_normals(verts, tris):
    v_array = np.array(verts)
    tri_array = np.array(tris, dtype=int)
    tri_pts = v_array[tri_array]
    n = np.cross( tri_pts[:,1] - tri_pts[:,0], 
                  tri_pts[:,2] - tri_pts[:,0])


    v_normals = np.zeros(v_array.shape)

    for i in range(tri_array.shape[0]):
        for j in tris[i]:
            v_normals[j,:] += n[i,:]

    nrms = np.sqrt(v_normals[:,0]**2 + v_normals[:,0]**2 + v_normals[:,2]**2)
    
    v_normals = v_normals / nrms.reshape((-1,1))

    return v_normals


def local_normalize_old(verts, signal, r=50):
    tree = cKDTree(verts)
    nsignal = np.zeros_like(signal)
    for i in range(len(verts)):
       idx = tree.query_ball_point(verts[i], r, eps=0.1)
       low = np.min(signal[idx])
       high = np.max(signal[idx])
       nsignal[i] = (signal[i]-low)/((high-low)+1e-6)
       if i%1000==0:
            print i, nsignal[i]
    return nsignal

def local_normalize(verts, signal, r=20):
    sub_idx = [i for i in range(len(verts)) if random()<0.001]
    sub_verts = [verts[i] for i in sub_idx]
    print "samples ", len(sub_verts)
    tree = cKDTree(verts)
    sub_med = np.zeros((len(sub_verts),), dtype=signal.dtype)
    sub_max = np.zeros((len(sub_verts),), dtype=signal.dtype)
    min_threshold = 1
    for i in range(len(sub_verts)):
        idx = tree.query_ball_point(sub_verts[i], r)
        d = [v for v in signal[idx] if v>min_threshold]
        if d:
            sub_med[i] = np.percentile(d, 40)#signal[idx])
            sub_max[i] = np.max(d)#signal[idx])
        else:
            sub_med[i] = min_threshold
            sub_max[i] = min_threshold
        if i%100==0:
            print '*', i, sub_med[i], sub_max[i]

    sub_med = np.maximum(min_threshold, sub_med)
    sub_max = np.maximum(min_threshold, sub_max)
 
    med_rbf = Rbf([_[0] for _ in sub_verts], [_[1] for _ in sub_verts], [_[2] for _ in sub_verts], sub_med, smooth=10, function='linear')

    #print heapy.heap()
    max_rbf = Rbf([_[0] for _ in sub_verts], [_[1] for _ in sub_verts], [_[2] for _ in sub_verts], sub_max, smooth=10, function='linear')


    nsignal = np.zeros(signal.shape, dtype=np.float32)
    for i in range(len(verts)):
        
        median = med_rbf(*verts[i])
        high = max_rbf(*verts[i])

        nsignal[i] = signal[i] - median #min(max((signal[i] - median)/(max(high - median, 10.0)), 0), 1)
        if i%1000==0:
            print i, nsignal[i], signal[i], median, high
    #nsignal = np.maximum((nsignal - median), 0)
    high = float(np.max(nsignal))
    low = float(np.min(nsignal))
    nsignal = 255*((nsignal-low)/(high-low))

    return nsignal.astype(np.uint8)

    

def triangulate_polygon(points, p, n):
    if abs(np.dot(n, [1,0,0]))>1e-3:
        s = np.array((1,0,0))
    else:
        s = np.array((0,1,0))

    x = np.cross(n, s)
    x = x/la.norm(x)
    y = np.cross(n, x)
    pp = [(np.dot(v,x), np.dot(v,y)) for v in points]
    return geom.ear_clip(pp)


def parse_ply2(filename):
    f = open(filename, 'r')
    #
    l = f.readline()
    if l[0:3]!='ply':
        raise RuntimeError('No ply magic string at beginning of file')
    l = f.readline()
    sl = l.split()
    if sl[0]!='format' or sl[1]!='ascii':
        raise RuntimeError('File not in ascii format')
    l = f.readline()
    while l.split()[0]=='comment':
        print l[l.index(' ')+1:]
        l = f.readline()
    element_list = []
    while l.strip()!='end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0]=='property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]
        eltype_data = []
        for i in range(el[1]):
            el_data = []
            l = f.readline()
            sl = l.split()
            buf = sl
            for prop in el[2]:
                prop_name = prop[0]
                prop_type = prop[1]
                if prop_type[0] == 'list':
                    count = int(buf[0])
                    if prop_type[2]=='float' or prop_type[2]=='double':
                        prop_data = map(float, buf[1:count+1])
                    else:
                        prop_data = map(int, buf[1:count+1])
                    buf = buf[count+1:]
                else:
                    if prop_type[0]=='float' or prop_type[0]=='double':
                        prop_data = float(buf[0])
                    else:
                        prop_data = int(buf[0])
                    buf = buf[1:]
                el_data.append(prop_data)
            eltype_data.append(tuple(el_data))
        data[eltype_name] = (eltype_data, [x[0] for x in el[2]])
    print data['face'][-1]

    print 'done_parse'
    return element_list, data


def write_ply2(filename, descr, data):
    f = open(filename, 'w')
    f.write('ply\nformat ascii 1.0\ncomment mesh project\n')
    for e in descr:
        count = len(data[e[0]][0])
        f.write('element '+e[0]+' '+str(count)+'\n')
        for p in e[2]:
            f.write('property '+' '.join(p[1])+' '+p[0]+'\n')
    f.write('end_header\n')
    for e in descr:
        edata = data[e[0]][0]
        for el in edata:
            op = []
            for i, p in enumerate(e[2]):
                v = el[i]
                if p[1][0]!='list':
                    op.append(v)
                else:
                    op.append(len(v))
                    op.extend(v)
            f.write(' '.join(map(str, op))+'\n')
    f.close()
                    
           

class Mesh(object):
    def __init__(self):
        self.verts = [] # array of vertex positions
        self.threshold = float("inf")

    def make_connectivity_matrix(self):
        n = len(self.verts)
        connections = {}
        for t in self.tris:
            connections[(t[0], t[1])] = 1
            connections[(t[1], t[0])] = 1
            connections[(t[1], t[2])] = 1
            connections[(t[2], t[1])] = 1
            connections[(t[2], t[0])] = 1
            connections[(t[0], t[2])] = 1
        A = dok_matrix((n,n))
        A.update(connections)
        A = A.tocsr()
        D = A.sum(axis=1)
        self.connectivity = A
        self.degree = D
        
    def smooth_signal(self, s, delta=0.1, iterations=1):
        s = s[:, np.newaxis]
        for i in range(iterations):
            s = (1-delta)*s + delta*self.connectivity.dot(s)/self.degree        
        s = np.squeeze(np.asarray(s))
        return s

    def connected_components(self, mask):
        print "begin cc"
        to_visit = set(i for i,v in enumerate(mask) if v>0)
        components = np.zeros(self.vert_signal.shape, dtype='int')
        cc = 1
        s = set()
        A = self.connectivity
        while to_visit:
            s.add(iter(to_visit).next())
            while s:
                i = s.pop()
                to_visit.remove(i)
                components[i] = cc
                neighbours = A.indices[A.indptr[i]:A.indptr[i+1]]
                for j in neighbours:
                    if j in to_visit:
                        s.add(j)
            #print cc, len(to_visit)
            cc +=1
        print "end cc", cc
        return components


    def find_local_minima(self, s):
        minima = []
        A = self.connectivity
        for i in range(len(self.verts)):
#            print A.indptr[i], A.indptr[i+1], s[0, A.indices[A.indptr[i]:A.indptr[i+1]]].shape
            min_other = np.amin(s[A.indices[A.indptr[i]:A.indptr[i+1]]])
            if s[i] < min_other:
                minima.append(i)
        return minima


    def load_ply2(self):
        descr, data = parse_ply2(sys.argv[1])
        self.descr = descr
        self.data = data
        NV = len(data['vertex'][0])
        NF = len(data['face'][0])
        print 'NF', NF
        verts = []
        vert_norms = []
        vert_signal = []
        vert_labels = []
        print data['vertex'][1]
        x_idx = data['vertex'][1].index('x')
        y_idx = data['vertex'][1].index('y')
        z_idx = data['vertex'][1].index('z')

        # Does the surface have normal data?
        if 'nx' in data['vertex'][1]:
            nx_idx = data['vertex'][1].index('nx')
            ny_idx = data['vertex'][1].index('ny')
            nz_idx = data['vertex'][1].index('nz')
            has_normal = True
        else:
            has_normal = False
        if 'signal' in data['vertex'][1]:
            s_idx = data['vertex'][1].index('signal')
        else:
            s_idx = data['vertex'][1].index('red')
#        l_idx = data['vertex'][1].index('red')
        for v in data['vertex'][0]:
            verts.append((v[x_idx], v[y_idx], v[z_idx]))
            if has_normal:
                vert_norms.append(np.array((v[nx_idx], v[ny_idx], v[nz_idx])))
            vert_signal.append(v[s_idx])
            vert_labels.append(0 if v[s_idx]>0 else 1)
        #del data['vertex']



        print 'done_vertex'



        v_array=np.array(verts,dtype='float32')
        v_array=v_array-np.sum(v_array,0)/len(v_array)
        bbox=(np.min(v_array,0),  np.max(v_array,0) )
        v_array=v_array-0.5*(bbox[0] + bbox[1])
        bbox=(np.min(v_array,0),  np.max(v_array,0) )
        zoom=1.0/la.norm(bbox[1])
        self.verts = [np.array(v) for v in v_array]
        self.vert_signal = np.array(vert_signal, dtype=np.float32)
        self.orig_vert_signal = np.array(vert_signal, dtype=np.float32)
        self.vert_labels = np.array(vert_labels, dtype=np.int32)
        self.tris = []
        for f in data['face'][0]:
            vv = f[0]
            tris = []
            for i in range(len(vv)-2):
                tris.append((vv[0], vv[i+1], vv[i+2]))

            self.tris.extend(tris)

        if not has_normal:
            print 'Calculate surface normals'
            # Area weighted surface normals (would prefer angle-weighted)
            self.vert_norms = calculate_vertex_normals(v_array, self.tris)
        else:
            self.vert_norms = np.array(vert_norms)
        

        print 'done_face'
        print 'make matrix'
        self.make_connectivity_matrix()
        self.calculate_vertex_areas()
        self.calculate_edge_perimeters()
#        print np.min(self.vert_signal), np.max(self.vert_signal), np.mean(self.vert_signal)
        print 'done matrix'
        return zoom


    """
    def generate_arrays_face(self):
        print 'start_arrays'
        npr.seed(1)
        tris = []
        tt = []
        cell_indices = {}
        for i,c in self.cells.iteritems():
            cell_indices[i] = (len(tris), len(c)) 
            tris.extend(c)
            tt.extend([i]*len(c))
#        del self.cells
        v_array=np.array(self.verts,dtype='float32') 
#        del self.verts
        tri_array=np.array(tris,dtype='i')

        v_out=v_array[tri_array.flatten()]
        tri_pts=v_array[tri_array]
        n = np.cross( tri_pts[:,1 ] - tri_pts[:,0], 
                   tri_pts[:,2 ] - tri_pts[:,0])
#        del tri_pts
        nrms = np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
        n=n/nrms.reshape((-1,1))
        n_out=np.repeat(n,3,axis=0)
        del n
        idx_out=np.arange(len(tris)*3,dtype='uint32')
        del tris

        self.col_map=(255*npr.random((max(tt)+1,3))).astype(np.uint8)

        cols=(self.col_map/255.0).astype(np.float32)
        col_out=cols[np.repeat(tt,3)]
#        col_out = (npr.random((3*len(tt),3))).astype(np.float32)
        print 'done_arrays'
        print v_out.shape, col_out.shape
        return v_out, n_out, col_out, idx_out, cell_indices
    """


    def generate_arrays(self):
        print 'start_arrays'
        npr.seed(1)
        tris = []
        v_out=np.array(self.verts,dtype=np.float32) 
        idx_out=np.array(self.tris,dtype=np.uint32)
        n_out=np.array(self.vert_norms,dtype=np.float32)
        nl = np.max(self.vert_labels)+1
        self.label_colmap=npr.random((nl,3))
        self.label_colmap[0,:] = 10.0/255.0
#        col_out = np.array(self.label_colmap[self.vert_labels, :], dtype=np.float32)
        nsignal = 0.1+ 0.9*(self.vert_signal / float(np.max(self.vert_signal)))
        col_out = np.tile(nsignal,(3,1)).T

        return v_out, n_out, col_out, idx_out

    def update_vertex_labels(self, newlabels):
        nl = np.max(newlabels)+1
        old_nl = self.label_colmap.shape[0]
        if nl>old_nl:
            new_colmap = np.zeros((nl, 3), dtype=np.float32)
            new_colmap[:old_nl,:] = self.label_colmap
            new_colmap[old_nl:] = npr.random((nl-old_nl, 3))
            self.label_colmap = new_colmap
        self.vert_labels = newlabels
        
    def view_vertex_labels(self, obj):
        col_out = np.array(self.label_colmap[self.vert_labels, :], dtype=np.float32)
        nsignal = 0.1+ 0.9*(self.vert_signal / float(np.max(self.vert_signal)))
        mask = self.vert_signal >= self.threshold
        
#        col_out = np.tile(nsignal,(3,1)).T
        col_signal = np.vstack((nsignal*mask, nsignal, nsignal*mask)).T

#        col_signal = np.tile(nsignal,(3,1)).T
        row_mask = self.vert_labels==0
        print np.max(nsignal)
        col_out[row_mask, :] = col_signal[row_mask,:]
        obj.vb[:,6:9] = col_out
        obj.vtVBO.bind()
        obj.vtVBO.set_array(obj.vb)
        obj.vtVBO.copy_data()

    def update_vertex_signal(self, newsignal):
        self.vert_signal = newsignal
        
    def update_threshold(self, threshold):
        self.threshold = threshold
        
    def view_vertex_signal(self, obj):
        nsignal = 0.1+ 0.9*(self.vert_signal / float(np.max(self.vert_signal)))
        print np.max(nsignal)
        mask = self.vert_signal >= self.threshold
        
#        col_out = np.tile(nsignal,(3,1)).T
        col_out = np.vstack((nsignal*mask, nsignal, nsignal*mask)).T
        print col_out[0, :]
        obj.vb[:,6:9] = col_out
        obj.vtVBO.bind()
        obj.vtVBO.set_array(obj.vb)
        obj.vtVBO.copy_data()


    def calculate_vertex_areas(self):
        n = len(self.verts)
        va = np.zeros((n,), dtype=np.float32)
        v_array=np.array(self.verts,dtype='float32') 
        tri_array=np.array(self.tris,dtype='i')
        tri_pts=v_array[tri_array]
        n = np.cross( tri_pts[:,1 ] - tri_pts[:,0], 
                   tri_pts[:,2 ] - tri_pts[:,0])
        ta = np.sqrt(n[:,0]**2+n[:,1]**2+n[:,2]**2)
        for t, a in zip(self.tris, ta):
            va[list(t)] += a/6.0
        self.vertex_area = va
        return va

    def calculate_edge_lengths(self):
        try:
            A = self.connectity
        except AttributeError:
            self.make_connectivity_matrix
            A = self.connectivity
        n = len(self.verts)
        d = []
        dd = {}
        for i in range(self.n):
            for j in A.indicies[A.indptr[i]:A.indptr[i+1]]:
                l = la.norm(verts[i] - verts[j])
                dd[(i,j)] = l
                d.append(la.norm(verts[i] - verts[j]))
        self.edge_lengths = d
        return dd

    def calculate_edge_perimeters(self):
        ep = defaultdict(float)
        n = len(self.verts)
        E = dok_matrix((n,n), dtype=np.float32)
        for t0, t1, t2 in self.tris:
            v0, v1, v2 = self.verts[t0], self.verts[t1], self.verts[t2]
            h0 = la.norm(v0 - 0.5*(v1+v2))/3.0
            h1 = la.norm(v1 - 0.5*(v0+v2))/3.0
            h2 = la.norm(v2 - 0.5*(v0+v1))/3.0
            ep[(t0, t1)] += h2
            ep[(t1, t0)] += h2
            ep[(t1, t2)] += h0
            ep[(t2, t1)] += h0
            ep[(t0, t2)] += h1
            ep[(t2, t0)] += h1

        E.update(ep)
        E = E.tocsr()
        self.edge_perimeters = E
        self.edge_perimeter_sums = np.squeeze(np.asarray(E.sum(axis=1)))
        return E


    def save_ply(self, filename, state):
        data = self.data
        descr = self.descr
        descr[0][2].append(('state',['int']))
        n = []
        for el, s in zip(data['vertex'][0], state):
            n.append(tuple(list(el)+[s]))
        data['vertex'] = (n, data['vertex'][1])

        write_ply2(filename, descr, data)
            

def translate(x,y,z):
    a = np.eye(4)
    a[0,3]=x
    a[1,3]=y
    a[2,3]=z
    return a

def scale(s):
    a = np.eye((4))
    a[0,0]=s
    a[1,1]=s
    a[2,2]=s
    a[3,3]=1.0
    return a

def perspective(fovy, aspect, zNear, zFar):
    f = 1.0/math.tan(fovy/2.0/180*math.pi)
    return np.array(((f/aspect, 0, 0, 0), (0,f,0,0), (0,0,(zFar+zNear)/(zNear-zFar), 2*zFar*zNear/(zNear-zFar)), (0, 0, -1, 0)))

def find_list(lst, pred):
    return next((i for i,v in enumerate(lst) if pred(v)),None)

def find_list(lst, pred):
    return next((i for i,v in enumerate(lst) if pred(v)),None)

class Obj:
    pass

class GLWidget(QGLWidget):
     
    def __init__(self, parent):
        QGLWidget.__init__(self, parent)
        self.setMinimumSize(500, 500)
        self.PMatrix = np.eye(4)
        self.VMatrix = np.eye(4)
        self.view_signal = False
        self.spm = None
        self.threshold = 10

    def mousePressEvent(self, event):
        self.ball.down([event.pos().x(), event.pos().y()])

    def mouseMoveEvent(self,event):
        x=event.pos().x()
        y=event.pos().y()
        self.ball.drag([x,y])
        print "move"
        self.repaint()

    def changeThreshold(self, value):
        print "new threshold", value
        self.threshold = value
        self.mesh.update_threshold(value)
        if self.view_signal:
            self.mesh.view_vertex_signal(self.obj[0])
        else:
            self.mesh.view_vertex_labels(self.obj[0])
        self.repaint()


    def keyPressEvent(self, event):
        k = event.key()
        self.handleKeyPress(k)
    
    def handleKeyPress(self, k):
        print "key"
        if k==QtCore.Qt.Key_B:
            self.mesh.update_vertex_signal(self.mesh.smooth_signal(self.mesh.vert_signal, 0.1, 1))
            if self.view_signal:
                self.mesh.view_vertex_signal(self.obj[0])
            else:
                self.mesh.view_vertex_labels(self.obj[0])
            self.repaint()


        if k==QtCore.Qt.Key_N:
            self.mesh.update_vertex_signal(local_normalize(self.mesh.verts, self.mesh.vert_signal))
            if self.view_signal:
                self.mesh.view_vertex_signal(self.obj[0])
            else:
                self.mesh.view_vertex_labels(self.obj[0])
            self.repaint()

        if k==QtCore.Qt.Key_R:
            self.mesh.update_vertex_signal(np.array(self.mesh.orig_vert_signal))
            if self.view_signal:
                self.mesh.view_vertex_signal(self.obj[0])
            else:
                self.mesh.view_vertex_labels(self.obj[0])
            self.repaint()

        if k==QtCore.Qt.Key_S:
            self.spm = MeshSPM(self.mesh, self.mesh.vert_signal.astype(np.uint8), self.threshold)
            self.mesh.update_vertex_labels(self.spm.state)
#            self.mesh.update_vertex_signal((self.spm.energy>0).astype(np.float32))
            self.view_signal = False
            self.mesh.view_vertex_labels(self.obj[0])
            self.old_threshold = self.threshold
            self.repaint()

        if k==QtCore.Qt.Key_T:
            threshold = self.threshold
            print "Threshold", threshold
            data = (self.mesh.vert_signal < threshold).astype(int)
            labels = data #self.mesh.connected_components(data)
#            print np.max(labels), np.min(labels), np.mean(labels)
            self.mesh.update_vertex_labels(labels)
            if self.spm:
                self.spm.SetState(self.mesh.vert_labels)

            self.view_signal = False
            self.mesh.view_vertex_labels(self.obj[0])
            self.repaint()

        if k==QtCore.Qt.Key_C:
            threshold = self.threshold
            print "Connected components", threshold
            data = self.mesh.vert_labels > 0
            labels = self.mesh.connected_components(data)
            print np.max(labels), np.min(labels), np.mean(labels)

            self.mesh.update_vertex_labels(labels)
            if self.spm:
                self.spm.SetState(self.mesh.vert_labels)

            self.view_signal = False
            self.mesh.view_vertex_labels(self.obj[0])
            self.repaint()

        if k==QtCore.Qt.Key_E:
            if self.spm:
                if self.old_threshold != self.threshold:
                    self.spm.SetThreshold(self.threshold)
                    self.old_threshold = self.threshold
            else:
                self.spm = MeshSPM(self.mesh, self.mesh.vert_signal.astype(np.uint8), self.threshold)
                self.spm.SetState(self.mesh.vert_labels)
                self.spm.SetThreshold(self.threshold)
                self.old_threshold = self.threshold
            self.spm.Run(100)
            self.mesh.update_vertex_labels(self.spm.state)
            self.view_signal = False
            self.mesh.view_vertex_labels(self.obj[0])
            self.repaint()
                


        if k==QtCore.Qt.Key_Z:
            erode_labels(self.mesh, self.mesh.vert_labels)
            self.view_signal = False
            self.mesh.view_vertex_labels(self.obj[0])
            if self.spm:
                self.spm.SetState(self.mesh.vert_labels)

            self.repaint()


        if k==QtCore.Qt.Key_F:
            if self.spm:
                self.mesh.save_ply(sys.argv[2], self.spm.state)
                self.repaint()

        if k==QtCore.Qt.Key_W:
            print "start watershed"
            l = surface_watershed(self.mesh, self.mesh.vert_signal, self.mesh.vert_labels, max_level=self.threshold)
            print "end watershed"
            self.mesh.update_vertex_labels(l)
            if self.spm:
                self.spm.SetState(self.mesh.vert_labels)
            self.view_signal = False
            self.mesh.view_vertex_labels(self.obj[0])
            self.repaint()            
                
  
    def wheelEvent(self, event):
	factor=1.41**(-event.delta()/240.0)
	self.dist=self.dist*factor
        self.repaint()

    def mouseDoubleClickEvent(self,event):
        x=event.pos().x()
        y=event.pos().y()
#        self.pick(x,self.height-y) 

        if self.view_signal:
            self.mesh.view_vertex_labels(self.obj[0])
        else:
            self.mesh.view_vertex_signal(self.obj[0])
        self.view_signal = not self.view_signal
        self.repaint()
 
    def paintGL(self):
        '''
        Drawing routine
        '''
        print "paint"
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
       # glDepthFunc(GL_NEVER)
        glPolygonMode(GL_FRONT, GL_FILL)
        self.VMatrix = translate(0, 0, -self.dist).dot(self.ball.matrix()).dot(scale(self.zoom))


        for o in self.obj:
            self.render_obj(o)



    def resizeGL(self, w, h):
        '''
        Resize the GL window 
        '''
        self.width=w
        self.height=h       
        glViewport(0, 0, self.width, self.height)

        self.PMatrix = perspective(40.0, float(w)/h, 0.1, 10000.0)

        self.ball.place([w/2,h/2],h/2)
    
    def initializeGL(self):
        '''
        Initialize GL
        '''
        
        # set viewing projection
        glClearColor(0.0, 0.0, 0.0, 1.0)
        

        vertex = compileShader(
            """
	    attribute vec3 position;
	    attribute vec3 color;
	    attribute vec3 normal;
            varying vec3 v_color;
            varying vec3 v_normal;
            uniform mat4 mv_matrix;
            uniform mat4 p_matrix;
	    void main() {
                vec4 eye =  mv_matrix * vec4(position,1.0);
		gl_Position = p_matrix * eye;
                v_normal = (mv_matrix * vec4(normal, 0.0)).xyz;
                v_color = color;
	    }""",
            GL_VERTEX_SHADER)

        fragment = compileShader(
            """
            const vec3 light_direction =  vec3(0., 0., -1.);
            const vec4 light_diffuse = vec4(0.5, 0.5, 0.5, 0.0);
            const vec4 light_ambient = vec4(0.5, 0.5, 0.5, 1.0);
            varying vec3 v_color;
            varying vec3 v_normal;
            uniform float alpha;
       
	    void main() {
                vec3 normal = normalize(v_normal); 
                vec4 diffuse_factor
                   = max(-dot(normal, light_direction),0.0) * light_diffuse;
                vec4 ambient_diffuse_factor
                   = diffuse_factor + light_ambient;
                gl_FragColor = ambient_diffuse_factor*vec4(v_color,alpha);
	    }""",
            GL_FRAGMENT_SHADER)
            
    

        self.shader = compileProgram(vertex,fragment)


        self.position_location = glGetAttribLocation( 
            self.shader, 'position' 
            )
        self.normal_location = glGetAttribLocation( 
            self.shader, 'normal' 
            )
        self.color_location = glGetAttribLocation( 
            self.shader, 'color' 
            )

        self.mv_location = glGetUniformLocation(
            self.shader, 'mv_matrix'
            )
        self.p_location = glGetUniformLocation(
            self.shader, 'p_matrix'
            )
        self.alpha_location = glGetUniformLocation(
            self.shader, 'alpha'
            )


        self.vStride = 9*4
        self.ball = Arcball()
        self.zoom = 1.0
        self.dist = 3.0
        self.obj=[self.load_ply()]
        self.mesh.view_vertex_labels(self.obj[0])

    def load_ply(self):

        m = Mesh()
        self.mesh = m
        self.zoom = m.load_ply2()
        v_out, n_out, col_out, idx_out = m.generate_arrays()


        vb=np.concatenate((v_out,n_out,col_out),axis=1)
        del v_out
        del n_out
        del col_out

        
        vao = np.array([0], dtype='uint32')
        vao[0] = glGenVertexArrays(1)

        glBindVertexArray(vao[0])
        obj=Obj()
        
        obj.vb = vb
        obj.vtVBO=VBO(vb)

        print 'made VBO', vb.shape, obj.vtVBO.shape

        obj.vtVBO.bind()
        obj.vao = vao

        glEnableVertexAttribArray( self.position_location )
        glVertexAttribPointer( 
            self.position_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO 
            )

        glEnableVertexAttribArray( self.normal_location )
        glVertexAttribPointer( 
            self.normal_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO+12 
            )			
        glEnableVertexAttribArray( self.color_location )
        glVertexAttribPointer( 
            self.color_location, 
            3, GL_FLOAT, False, self.vStride, obj.vtVBO+24 
            )

        glBindVertexArray( 0 )
        glDisableVertexAttribArray( self.position_location )
        glDisableVertexAttribArray( self.normal_location )
        glDisableVertexAttribArray( self.color_location )
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        obj.elVBO=VBO(idx_out, target=GL_ELEMENT_ARRAY_BUFFER)
        obj.elCount=len(idx_out.flatten())
        obj.vao = vao[0]


        glBindVertexArray( 0 )
        glDisableVertexAttribArray( self.position_location )
        glDisableVertexAttribArray( self.normal_location )
        glDisableVertexAttribArray( self.color_location )
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        obj.elVBO=VBO(idx_out, target=GL_ELEMENT_ARRAY_BUFFER)
        obj.elCount=len(idx_out.flatten())

        del idx_out



        print 'made obj'
        return obj


#    @profile
    def render_obj(self, obj):

        glEnable(GL_CULL_FACE)
        glDepthMask(True)

        glUseProgram(self.shader)
#        obj.vtVBO.bind()
        glBindVertexArray( obj.vao )
        print "copied", obj.elVBO.copied
        obj.elVBO.bind()

        glUniformMatrix4fv(self.mv_location, 1, True, self.VMatrix.astype('float32'))
        glUniformMatrix4fv(self.p_location, 1, True, self.PMatrix.astype('float32'))

        

        print 'start_draw'
        glDrawElements(
                GL_TRIANGLES, obj.elCount,
                GL_UNSIGNED_INT, obj.elVBO
            )
        print 'done_draw'



        obj.elVBO.unbind()
        glBindVertexArray( 0 )
        glUseProgram(0)


class ToolBar(QtGui.QToolBar):
    def __init__(self):
        QtGui.QToolBar.__init__(self)
        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(256)
        self.slider.setValue(10)
        
        self.addWidget(self.slider)


class WidgetDemo(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.tbar = ToolBar()
        self.addToolBar(self.tbar)
        self.widget = GLWidget(self)    
        self.setCentralWidget(self.widget)
        self.tbar.slider.valueChanged.connect(self.widget.changeThreshold)

        self.widget.setFocus()

    def keyPressEvent(self, event):
        k = event.key()
        self.widget.handleKeyPress(k)




if __name__ == '__main__':
    app = QtGui.QApplication(['Python GL'])
    window = WidgetDemo()
    window.show()
    app.exec_()


                    
                    
            
        
        
    
