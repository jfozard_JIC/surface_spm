
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext_modules=[
    Extension("cpm_step",
              sources=["cpm_step.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step_weighted",
              sources=["cpm_step_weighted.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step_ws",
              sources=["cpm_step_ws.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              libraries=["m"] # Unix-like specific
    ),
]

setup(
  name = "cpm_step",
  ext_modules = cythonize(ext_modules)
)
