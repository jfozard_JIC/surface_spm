
# SPM on irregular triangulated surfaces

## Introduction

The Segementation Potts Model (SPM) provides an method for the segmentation of
images, representing individual cells, in which the boundaries between cells are
visible. This method is particularly effective when the boundaries between cells 
have holes which are smaller than the typical radius of curvature of the cell boundary;
this is often the case for confocal images of cell leaves.

Current implementations of the SPM only operate on a regular square or
cubical lattice, as images from confocal microscopy are normally obtained in this format.
Note, in particular, that the method currently assumes the the voxels for three-dimensional
data are roughly cubical; this is not necessarily the case for confocal images, where 
the spacing in the z-direction is often significantly different to that in the x- and y-
directions.

In some circumstances, such as the uppermost surface of a leaf, or a shoot meristem, the
image data of interest may lie on a curved two dimensional surface. Whilst techniques exist
to project such a curved surface onto a plane, as with making a map of the world, such projections
necessarily involve introducing distortion and/or cutting the surface. This causes difficulty in
applying the SPM model. Inspired by the watershed method on surfaces implemented in the
MorphographX software, it seems preferable to reformulate the SPM directly on the curved surface.

Whilst a two-dimensional surface embedded in three-dimensional space can be described in many different ways,
for the purposes of the SPM/CPM a discrete representation is required; one common and relatively simple
form is a triangulated mesh. 

## CPM steps on a triangulated mesh






